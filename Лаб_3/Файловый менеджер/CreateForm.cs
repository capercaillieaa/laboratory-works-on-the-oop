﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Файловый_менеджер
{
    public partial class CreateForm : Form
    {
        public string type;
        public string nameNewFile;
        
        public CreateForm()
        {
            InitializeComponent();
        }

        private void CreateForm_Shown(object sender, EventArgs e)
        {
            InputName.Text += type;
        }

        private void OKButton_Click(object sender, EventArgs e)
        {
            nameNewFile = NewName.Text;
        }

        private void NewName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                nameNewFile = NewName.Text;
                DialogResult = DialogResult.OK;
            }
        }
    }
}
