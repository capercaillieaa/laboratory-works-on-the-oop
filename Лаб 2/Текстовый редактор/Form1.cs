using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Текстовый_редактор
{
    public partial class TextEditor : Form
    {

        private bool checkSave = false;
        private bool checkEdit = false;

        public TextEditor()
        {
            InitializeComponent();
        }

        private void открытьToolStripButton_Click(object sender, EventArgs e)
        {
            if (!checkSave && checkEdit)
            {
                if (MessageBox.Show("Сохранить изменения?", "", MessageBoxButtons.YesNo) == DialogResult.Yes) Save();
            }
            TextEdit.Clear();
            Text = "Текстовый редактор";
            openFileDialog1.Filter = "RTF files|*.rtf";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                TextEdit.LoadFile(openFileDialog1.FileName);
                Text = openFileDialog1.FileName;
            }
            checkSave = false;
            checkEdit = false;
        }

        private void сохранитьToolStripButton_Click(object sender, EventArgs e)
        {
            Save();
        }

        private void создатьToolStripButton_Click(object sender, EventArgs e)
        {
            if (!checkSave && checkEdit)
            {
                if (MessageBox.Show("Сохранить изменения?", "", MessageBoxButtons.YesNo) == DialogResult.Yes) Save();
            }
            TextEdit.Clear();
            Text = "Текстовый редактор";
            checkSave = false;
            checkEdit = false;
        }

        private void справкаToolStripButton_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Разработчик: Глушков Андрей Александрович");
        }

        private void TextEdit_SelectionChanged(object sender, EventArgs e)
        {
            if (TextEdit.SelectionFont == null) return;
            BoldButton.Checked = TextEdit.SelectionFont.Bold;
            ItalicButton.Checked = TextEdit.SelectionFont.Italic;
            UnderlineButton.Checked = TextEdit.SelectionFont.Underline;
            if (TextEdit.SelectionAlignment == HorizontalAlignment.Left)
            {
                CenterButton.Checked = поЦентруToolStripMenuItem.Checked = false;
                RightButton.Checked = поПравомуКраюToolStripMenuItem.Checked = false;
                LeftButton.Checked = поЛевомуКраюToolStripMenuItem.Checked = true;
            }
            else if (TextEdit.SelectionAlignment == HorizontalAlignment.Center)
            {
                CenterButton.Checked = поЦентруToolStripMenuItem.Checked = true;
                RightButton.Checked = поПравомуКраюToolStripMenuItem.Checked = false;
                LeftButton.Checked = поЛевомуКраюToolStripMenuItem.Checked = false;
            }
            else if (TextEdit.SelectionAlignment == HorizontalAlignment.Right)
            {
                CenterButton.Checked = поЦентруToolStripMenuItem.Checked = false;
                RightButton.Checked = поПравомуКраюToolStripMenuItem.Checked = true;
                LeftButton.Checked = поЛевомуКраюToolStripMenuItem.Checked = false;
            }
            SizeComboBox.Text = TextEdit.SelectionFont.Size.ToString();
        }

        private void Save()
        {
            saveFileDialog1.Filter = "RTF files|*.rtf";
            if (Text != "Текстовый редактор")
            {
                checkSave = true;
                TextEdit.SaveFile(Text);
                Text = openFileDialog1.FileName;
            }
            else if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
 INDX( 	 � }            (   �  �      � ��x                +     ` N     0     ?�o������������������������                       5 9 F 2 ~ 1 . 3     h V     0     1 5�����Be����Be���1 5���� �       �              
A 2 3 3 ~ 1 . P D B 51     h V     0     /������Ke�����Ke���/���� `       Z              
B E D 4 ~ 1 . E X E 54     p Z     0     m�s�����\蘑���\蘑��m�s���� `      �X              V S H O S T ~ 1 . E X E 546+     ` L     0     ?�o�� ���������������������                       "5ABK2K1     � l     0     /������Ke�����Ke���/���� `       Z              $09;>2K9  <5=5465@. e x e     �     � z     0     !���i��������������!���i���       �                $09;>2K9  <5=5465@. e x e . c o n f i g z     3     � l     0     1 5�����Be����Be���1 5���� �       �              $09;>2K9  <5=5465@. p d b     4     � z     0     m�s�����\蘑���\蘑� m�s���� `      �X              $09;>2K9  <5=5465@. v s h o s t . e x e       �     � �     0     ����i������������������i���       �               # $09;>2K9  <5=5465@. v s h o s t . e x e . c o n f i g �     � �     0     ����i��2g�����y��]r�����i���      �              % $09;>2K9  <5=5465@. v s h o s t . e x e . m a n i f e s t                  ����i���      �              % $09;>2K9  <5=5465@. v s h o s t . e � e . m a n i f e s t                          % $09;>2K9  <5=5465@. v s h o s t . e x e . m a n i f e s t                  �              % $09;>2K9  <5=5465@. v s h o s t . e x e . m a n i f e s t                  % $09;>2K9  <5=5465@. v s h o s t . e x e . m a n i f e s t                  ����i����Nl��{�5�V�����i���      �              % $09;>2K9  <5=5465@. v s h o s t . e x e . m a n i f e s t                  �    � � �     0     ����i����Nl��{�5�V�����i���      �              % $09;>2K9  <5=5465@. v s h o s t . e x e . m a n i f e s t                                                                                                                                                                                                                                                                                                                                                                        �                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               �                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               �                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               � {
                if (UnderlineButton.Checked)
                {
                    TextEdit.SelectionFont = new Font(TextEdit.SelectionFont, TextEdit.SelectionFont.Style | FontStyle.Underline);
                    подчеркнутыйToolStripMenuItem.Checked = true;
                }
                else
                {
                    TextEdit.SelectionFont = new Font(TextEdit.SelectionFont, TextEdit.SelectionFont.Style & ~FontStyle.Underline);
                    подчеркнутыйToolStripMenuItem.Checked = false;
                }
            }
            else
            {
                int startIndex = TextEdit.SelectionStart;
                int selectionSize = TextEdit.SelectionLength;
                TextEdit.SelectionLength = 1;

                for (int i = startIndex; i < startIndex + selectionSize; i++)
                {
                    if (check)
                    {
                        TextEdit.SelectionFont = new Font(TextEdit.SelectionFont, TextEdit.SelectionFont.Style | FontStyle.Underline);
                        //подчеркнутыйToolStripMenuItem.Checked = true;
                    }
                    else
                    {
                        TextEdit.SelectionFont = new Font(TextEdit.SelectionFont, TextEdit.SelectionFont.Style & ~FontStyle.Underline);
                        //подчеркнутыйToolStripMenuItem.Checked = false;
                    }
                    TextEdit.SelectionStart++;
                }
                TextEdit.SelectionStart = startIndex;
                TextEdit.SelectionLength = selectionSize;
            }
        }

        private void SizeComboBox_DropDownClosed(object sender, EventArgs e)
        {
            int startIndex = TextEdit.SelectionStart;
            int selectionLength = TextEdit.SelectionLength;
            int selectionSize = Convert.ToInt16(SizeComboBox.SelectedItem);
            TextEdit.SelectionLength = 1;

            for (int i = startIndex; i < startIndex + selectionLength; i++)
            {
                try
                {
                    TextEdit.SelectionFont = new Font(TextEdit.SelectionFont.FontFamily, selectionSize, TextEdit.SelectionFont.Style);
                    TextEdit.Focus();
                }
                catch
                {
                    MessageBox.Show("Неподдерживаемый шрифт.\nРазмер изменить нельзя");
                }
                TextEdit.SelectionStart++;
            }
            TextEdit.SelectionStart = startIndex;
            TextEdit.SelectionLength = selectionLength;
        }

        private void SizeComboBox_TextUpdate(object sender, EventArgs e)
        {
            int startIndex = TextEdit.SelectionStart;
            int selectionLength = TextEdit.SelectionLength;
            int selectionSize = Convert.ToInt16(SizeComboBox.SelectedItem);
            TextEdit.SelectionLength = 1;

            for (int i = startIndex; i < startIndex + selectionLength; i++)
            {
                try
                {
                    TextEdit.SelectionFont = new Font(TextEdit.SelectionFont.FontFamily, selectionSize);
                    TextEdit.Focus();
                }
                catch
                {
                    MessageBox.Show("Неподдерживаемый шрифт.\nРазмер изменить нельзя");
                }
                TextEdit.SelectionStart++;
            }
            TextEdit.SelectionStart = startIndex;
            TextEdit.SelectionLength = selectionLength;
        }

        private void TextColorButton_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                TextEdit.SelectionColor = colorDialog1.Color;
            }
        }

        private void FontButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (fontDialog1.ShowDialog() == DialogResult.OK)
                {
                    TextEdit.SelectionFont = fontDialog1.Font;
                    SizeComboBox.Text = (fontDialog1.Font.Size.ToString());
                    BoldButton.Checked = TextEdit.SelectionFont.Bold;
                    ItalicButton.Checked = TextEdit.SelectionFont.Italic;
                    UnderlineButton.Checked = TextEdit.SelectionFont.Underline;
                }
            }
            catch { }
        }

        private void BackcolorButton_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                TextEdit.SelectionBackColor = colorDialog1.Color;
            }
        }

        private void CenterButton_Click(object sender, EventArgs e)
        {
            if (TextEdit.SelectionAlignment == HorizontalAlignment.Center)
            {
                CenterButton.Checked = поЦентруToolStripMenuItem.Checked = false;
                LeftButton.Checked = поЛевомуКраюToolStripMenuItem.Checked = true;
                TextEdit.SelectionAlignment = HorizontalAlignment.Left;
            }
            else
            {
                CenterButton.Checked = поЦентруToolStripMenuItem.Checked = true;
                RightButton.Checked = поПравомуКраюToolStripMenuItem.Checked = false;
                LeftButton.Checked = поЛевомуКраюToolStripMenuItem.Checked = false;
                TextEdit.SelectionAlignment = HorizontalAlignment.Center;
            }
        }

        private void LeftButton_Click(object sender, EventArgs e)
        {
            if (TextEdit.SelectionAlignment != HorizontalAlignment.Left)
            {
                CenterButton.Checked = поЦентруToolStripMenuItem.Checked = false;
                RightButton.Checked = поПравомуКраюToolStripMenuItem.Checked = false;
                LeftButton.Checked = поЛевомуКраюToolStripMenuItem.Checked = true;
                TextEdit.SelectionAlignment = HorizontalAlignment.Left;
            }
            else
            {
                TextEdit.SelectionAlignment = HorizontalAlignment.Left;
                LeftButton.Checked = поЛевомуКраюToolStripMenuItem.Checked = true;
            }
        }

        private void RightButton_Click(object sender, EventArgs e)
        {
            if (TextEdit.SelectionAlignment == HorizontalAlignment.Right)
            {
                RightButton.Checked = поПравомуКраюToolStripMenuItem.Checked = false;
                LeftButton.Checked = поЛевомуКраюToolStripMenuItem.Checked = true;
                TextEdit.SelectionAlignment = HorizontalAlignment.Left;
            }
            else
            {
                CenterButton.Checked = поЦентруToolStripMenuItem.Checked = false;
                RightButton.Checked = поПравомуКраюToolStripMenuItem.Checked = true;
                LeftButton.Checked = поЛевомуКраюToolStripMenuItem.Checked = false;
                TextEdit.SelectionAlignment = HorizontalAlignment.Right;
            }
        }

        private void вырезатьToolStripButton_Click(object sender, EventArgs e)
        {
            TextEdit.Cut();
        }

        private void копироватьToolStripButton_Click(object sender, EventArgs e)
        {
            TextEdit.Copy();
            вставкаToolStripButton.Enabled = true;
        }

        private void вставкаToolStripButton_Click(object sender, EventArgs e)
        {
            TextEdit.Paste();
        }

        private void сохранитьКакToolStripMenuItem_Click(object sender, EventArgs e)
        {  
            saveFileDialog1.Filter = "RTF files|*.rtf";
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                checkSave = true;
                TextEdit.SaveFile(saveFileDialog1.FileName)-ой курс\4 семестр\ООП\Лаб 2\Текстовый редактор\obj\Debug\Текстовый_редактор.Properties.Resources.resources
J:\ВятГУ\2-ой курс\4 семестр\ООП\Лаб 2\Текстовый редактор\obj\Debug\Текстовый редактор.csproj.GenerateResource.Cache
F:\ВятГУ\2-ой курс\4 семестр\ООП\Лаб 2\Текстовый редактор\bin\Debug\Текстовый редактор.exe.config
F:\ВятГУ\2-ой курс\4 семестр\ООП\Лаб 2\Текстовый редактор\obj\Debug\Текстовый редактор.exe
F:\ВятГУ\2-ой курс\4 семестр\ООП\Лаб 2\Текстовый редактор\obj\Debug\Текстовый редактор.pdb
F:\ВятГУ\2-ой курс\4 семестр\ООП\Лаб 2\Текстовый редактор\bin\Debug\Текстовый редактор.exe
F:\ВятГУ\2-ой курс\4 семестр\ООП\Лаб 2\Текстовый редактор\bin\Debug\Текстовый редактор.pdb
F:\ВятГУ\2-ой курс\4 семестр\ООП\Лаб 2\Текстовый редактор\obj\Debug\Текстовый редактор.csprojResolveAssemblyReference.cache
F:\ВятГУ\2-ой курс\4 семестр\ООП\Лаб 2\Текстовый редактор\obj\Debug\Текстовый_редактор.TextEditor.resources
F:\ВятГУ\2-ой курс\4 семестр\ООП\Лаб 2\Текстовый редактор\obj\Debug\Текстовый_редактор.Properties.Resources.resources
F:\ВятГУ\2-ой курс\4 семестр\ООП\Лаб 2\Текстовый редактор\obj\Debug\Текстовый редактор.csproj.GenerateResource.Cache
D:\ВятГУ\2-ой курс\4 семестр\ООП\Лаб 2\Текстовый редактор\bin\Debug\Текстовый редактор.exe.config
D:\ВятГУ\2-ой курс\4 семестр\ООП\Лаб 2\Текстовый редактор\obj\Debug\Текстовый редактор.exe
D:\ВятГУ\2-ой курс\4 семестр\ООП\Лаб 2\Текстовый редактор\obj\Debug\Текстовый редактор.pdb
G:\ВятГУ\2-ой курс\4 семестр\ООП\Лаб 2\Текстовый редактор\bin\Debug\Текстовый редактор.exe.config
G:\ВятГУ\2-ой курс\4 семестр\ООП\Лаб 2\Текстовый редактор\obj\Debug\Текстовый редактор.exe
G:\ВятГУ\2-ой курс\4 семестр\ООП\Лаб 2\Текстовый редактор\obj\Debug\Текстовый редактор.pdb
G:\ВятГУ\2-ой курс\4 семестр\ООП\Лаб 2\Текстовый редактор\bin\Debug\Текстовый редактор.exe
G:\ВятГУ\2-ой курс\4 семестр\ООП\Лаб 2\Текстовый редактор\bin\Debug\Текстовый редактор.pdb
G:\ВятГУ\2-ой курс\4 семестр\ООП\Лаб 2\Текстовый редактор\obj\Debug\Текстовый редактор.csprojResolveAssemblyReference.cache
G:\ВятГУ\2-ой курс\4 семестр\ООП\Лаб 2\Текстовый редактор\obj\Debug\Текстовый_редактор.TextEditor.resources
G:\ВятГУ\2-ой курс\4 семестр\ООП\Лаб 2\Текстовый редактор\obj\Debug\Текстовый_редактор.Properties.Resources.resources
G:\ВятГУ\2-ой курс\4 семестр\ООП\Лаб 2\Текстовый редактор\obj\Debug\Текстовый редактор.csproj.GenerateResource.Cache
D:\ВятГУ\2-ой курс\4 семестр\ООП\Лаб 2\Текстовый редактор\bin\Debug\Текстовый редактор.exe
D:\ВятГУ\2-ой курс\4 семестр\ООП\Лаб 2\Текстовый редактор\bin\Debug\Текстовый редактор.pdb
D:\ВятГУ\2-ой курс\4 семестр\ООП\Лаб 2\Текстовый редактор\obj\Debug\Текстовый редактор.csprojResolveAssemblyReference.cache
D:\ВятГУ\2-ой курс\4 семестр\ООП\Лаб 2\Текстовый редактор\obj\Debug\Текстовый_редактор.TextEditor.resources
D:\ВятГУ\2-ой курс\4 семестр\ООП\Лаб 2\Текстовый редактор\obj\Debug\Текстовый_редактор.Properties.Resources.resources
D:\ВятГУ\2-ой курс\4 семестр\ООП\Лаб 2\Текстовый редактор\obj\Debug\Текстовый редактор.csproj.GenerateResource.Cache
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   