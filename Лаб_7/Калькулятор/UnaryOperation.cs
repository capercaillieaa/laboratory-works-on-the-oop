﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Калькулятор
{
    class UnaryOperation
    {
        readonly double a;
        double result;

        public UnaryOperation(double a)
        {
            if (double.IsInfinity(a))
            {
                MessageBox.Show("Невозможно выполнить действия с бесконечностью",
                "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.a = 0;
            }
            else if (double.IsNaN(a))
            {
                MessageBox.Show("Невозможно выполнить действия с числом формата NaN",
                "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.a = 0;
            }
            else
                this.a = a;
        }

        public double PerformOperation(object value)
        {
            switch (value)
            {
                case "1/x": { result = 1 / a; break; }
                case "x^2": { result = Math.Pow(a, 2); break; }
                case "sqrt": { result = Math.Sqrt(a); break; }              
                case "!": { result = Factorial(a); break; }
                case "e^x": { result = Math.Exp(a); break; }
                case "not": { result = double.Parse(NotOperation(a.ToString())); break; }
                default: { PerformTrigonometryFunction(value); break; }
            }
            return result;
        }

        private void PerformTrigonometryFunction(object value)
        {
            switch (value)
            {
                case "sin": { result = Math.Sin(a); break; }
                case "cos": { result = Math.Cos(a); break; }
                case "tg": { result = Math.Tan(a); break; }
            }
        }

        private string NotOperation(string value)
        {
            string result = "";
            foreach (var i in value)
            {
                if (i == '0') result += '1';
                else result += '0';
            }
            return result;
        }

        private double Factorial(double a)
        {
            double answer = 1;
            for (int i = 1; i <= a; i++)
                answer *= i;
            return answer;
        }
    }
}
