﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Калькулятор
{
    class BinaryOperation
    {
        readonly double a, b;
        public BinaryOperation(double a, double b)
        {
            if (double.IsInfinity(a) || double.IsInfinity(b))
            {
                MessageBox.Show("Невозможно выполнить действия с бесконечностью",
                "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.a = 0;
            }
            else if (double.IsNaN(a) || double.IsNaN(b))
            {
                MessageBox.Show("Невозможно выполнить действия с числом формата NaN",
                "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.a = 0;
            }
            else
            {
                this.a = a;
                this.b = b;
            }
        }

        public double PerformOperation(object value)
        {
            double result;
            switch (value)
            {
                case "+": { result = a + b; break; }
                case "-": { result = a - b; break; }
                case "*": { result = a * b; break; }
                case "/": { result = a / b; break; }
                case "x^n": { result = Math.Pow(a, b); break; }
                default: { result = double.Parse(PerformBitOperation(value)); break; }
            }
            return result;
        }

        private string PerformBitOperation(object value)
        {
            int result = 0;
            var translatorToDec = new Translator(2, 10);
            int number1 = int.Parse(translatorToDec.Translate(a.ToString()));
            int number2 = int.Parse(translatorToDec.Translate(b.ToString()));
            switch (value)
            {
                case "and": { result = number1 & number2; break; }
                case "or": { result = number1 | number2; break; }
            }
            return Convert.ToString(result, 2);
        }
    }
}
