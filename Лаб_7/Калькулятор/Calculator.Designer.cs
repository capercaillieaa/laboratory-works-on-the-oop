﻿namespace Калькулятор
{
    partial class Calculator
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.NineButton = new System.Windows.Forms.Button();
            this.EightButton = new System.Windows.Forms.Button();
            this.SevenButton = new System.Windows.Forms.Button();
            this.SixButton = new System.Windows.Forms.Button();
            this.FiveButton = new System.Windows.Forms.Button();
            this.FourButton = new System.Windows.Forms.Button();
            this.ThreeButton = new System.Windows.Forms.Button();
            this.TwoButton = new System.Windows.Forms.Button();
            this.OneButton = new System.Windows.Forms.Button();
            this.ZeroButton = new System.Windows.Forms.Button();
            this.BackspaceButton = new System.Windows.Forms.Button();
            this.CancelButton = new System.Windows.Forms.Button();
            this.EqualButton = new System.Windows.Forms.Button();
            this.PointButton = new System.Windows.Forms.Button();
            this.DivButton = new System.Windows.Forms.Button();
            this.MultiplyButton = new System.Windows.Forms.Button();
            this.SubtractButton = new System.Windows.Forms.Button();
            this.AddButton = new System.Windows.Forms.Button();
            this.FactorialButton = new System.Windows.Forms.Button();
            this.ReverseButton = new System.Windows.Forms.Button();
            this.SignumButton = new System.Windows.Forms.Button();
            this.expXButton = new System.Windows.Forms.Button();
            this.SqrButton = new System.Windows.Forms.Button();
            this.SqrtButton = new System.Windows.Forms.Button();
            this.HexButton = new System.Windows.Forms.RadioButton();
            this.DecButton = new System.Windows.Forms.RadioButton();
            this.OctButton = new System.Windows.Forms.RadioButton();
            this.BinButton = new System.Windows.Forms.RadioButton();
            this.HexEButton = new System.Windows.Forms.Button();
            this.HexDButton = new System.Windows.Forms.Button();
            this.HexCButton = new System.Windows.Forms.Button();
            this.HexBButton = new System.Windows.Forms.Button();
            this.HexAButton = new System.Windows.Forms.Button();
            this.HexFButton = new System.Windows.Forms.Button();
            this.andButton = new System.Windows.Forms.Button();
            this.orButton = new System.Windows.Forms.Button();
            this.notButton = new System.Windows.Forms.Button();
            this.PiButton = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.sinButton = new System.Windows.Forms.Button();
            this.expButton = new System.Windows.Forms.Button();
            this.tgButton = new System.Windows.Forms.Button();
            this.cosButton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.NumberTextBox = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // NineButton
            // 
            this.NineButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NineButton.BackColor = System.Drawing.Color.DarkKhaki;
            this.NineButton.FlatAppearance.BorderColor = System.Drawing.Color.Cornsilk;
            this.NineButton.FlatAppearance.BorderSize = 2;
            this.NineButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Cornsilk;
            this.NineButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Cornsilk;
            this.NineButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.NineButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NineButton.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.NineButton.Location = new System.Drawing.Point(300, 104);
            this.NineButton.Name = "NineButton";
            this.NineButton.Size = new System.Drawing.Size(50, 50);
            this.NineButton.TabIndex = 9;
            this.NineButton.TabStop = false;
            this.NineButton.Text = "9";
            this.NineButton.UseVisualStyleBackColor = false;
            this.NineButton.Click += new System.EventHandler(this.ZeroButton_Click);
            // 
            // EightButton
            // 
            this.EightButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.EightButton.BackColor = System.Drawing.Color.DarkKhaki;
            this.EightButton.FlatAppearance.BorderColor = System.Drawing.Color.Cornsilk;
            this.EightButton.FlatAppearance.BorderSize = 2;
            this.EightButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Cornsilk;
            this.EightButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Cornsilk;
            this.EightButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.EightButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.EightButton.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.EightButton.Location = new System.Drawing.Point(244, 104);
            this.EightButton.Name = "EightButton";
            this.EightButton.Size = new System.Drawing.Size(50, 50);
            this.EightButton.TabIndex = 8;
            this.EightButton.TabStop = false;
            this.EightButton.Text = "8";
            this.EightButton.UseVisualStyleBackColor = false;
            this.EightButton.Click += new System.EventHandler(this.ZeroButton_Click);
            // 
            // SevenButton
            // 
            this.SevenButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SevenButton.BackColor = System.Drawing.Color.DarkKhaki;
            this.SevenButton.FlatAppearance.BorderColor = System.Drawing.Color.Cornsilk;
            this.SevenButton.FlatAppearance.BorderSize = 2;
            this.SevenButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Cornsilk;
            this.SevenButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Cornsilk;
            this.SevenButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SevenButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.SevenButton.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.SevenButton.Location = new System.Drawing.Point(188, 104);
            this.SevenButton.Name = "SevenButton";
            this.SevenButton.Size = new System.Drawing.Size(50, 50);
            this.SevenButton.TabIndex = 7;
            this.SevenButton.TabStop = false;
            this.SevenButton.Text = "7";
            this.SevenButton.UseVisualStyleBackColor = false;
            this.SevenButton.Click += new System.EventHandler(this.ZeroButton_Click);
            // 
            // SixButton
            // 
            this.SixButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SixButton.BackColor = System.Drawing.Color.DarkKhaki;
            this.SixButton.FlatAppearance.BorderColor = System.Drawing.Color.Cornsilk;
            this.SixButton.FlatAppearance.BorderSize = 2;
            this.SixButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Cornsilk;
            this.SixButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Cornsilk;
            this.SixButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SixButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.SixButton.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.SixButton.Location = new System.Drawing.Point(300, 165);
            this.SixButton.Name = "SixButton";
            this.SixButton.Size = new System.Drawing.Size(50, 50);
            this.SixButton.TabIndex = 6;
            this.SixButton.TabStop = false;
            this.SixButton.Text = "6";
            this.SixButton.UseVisualStyleBackColor = false;
            this.SixButton.Click += new System.EventHandler(this.ZeroButton_Click);
            // 
            // FiveButton
            // 
            this.FiveButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FiveButton.BackColor = System.Drawing.Color.DarkKhaki;
            this.FiveButton.FlatAppearance.BorderColor = System.Drawing.Color.Cornsilk;
            this.FiveButton.FlatAppearance.BorderSize = 2;
            this.FiveButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Cornsilk;
            this.FiveButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Cornsilk;
            this.FiveButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.FiveButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FiveButton.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.FiveButton.Location = new System.Drawing.Point(244, 165);
            this.FiveButton.Name = "FiveButton";
            this.FiveButton.Size = new System.Drawing.Size(50, 50);
            this.FiveButton.TabIndex = 5;
            this.FiveButton.TabStop = false;
            this.FiveButton.Text = "5";
            this.FiveButton.UseVisualStyleBackColor = false;
            this.FiveButton.Click += new System.EventHandler(this.ZeroButton_Click);
            // 
            // FourButton
            // 
            this.FourButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FourButton.BackColor = System.Drawing.Color.DarkKhaki;
            this.FourButton.FlatAppearance.BorderColor = System.Drawing.Color.Cornsilk;
            this.FourButton.FlatAppearance.BorderSize = 2;
            this.FourButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Cornsilk;
            this.FourButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Cornsilk;
            this.FourButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.FourButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FourButton.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.FourButton.Location = new System.Drawing.Point(188, 165);
            this.FourButton.Name = "FourButton";
            this.FourButton.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.FourButton.Size = new System.Drawing.Size(50, 50);
            this.FourButton.TabIndex = 4;
            this.FourButton.TabStop = false;
            this.FourButton.Text = "4";
            this.FourButton.UseVisualStyleBackColor = false;
            this.FourButton.Click += new System.EventHandler(this.ZeroButton_Click);
            // 
            // ThreeButton
            // 
            this.ThreeButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ThreeButton.BackColor = System.Drawing.Color.DarkKhaki;
            this.ThreeButton.FlatAppearance.BorderColor = System.Drawing.Color.Cornsilk;
            this.ThreeButton.FlatAppearance.BorderSize = 2;
            this.ThreeButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Cornsilk;
            this.ThreeButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Cornsilk;
            this.ThreeButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ThreeButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ThreeButton.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.ThreeButton.Location = new System.Drawing.Point(300, 230);
            this.ThreeButton.Name = "ThreeButton";
            this.ThreeButton.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ThreeButton.Size = new System.Drawing.Size(50, 50);
            this.ThreeButton.TabIndex = 3;
            this.ThreeButton.TabStop = false;
            this.ThreeButton.Text = "3";
            this.ThreeButton.UseVisualStyleBackColor = false;
            this.ThreeButton.Click += new System.EventHandler(this.ZeroButton_Click);
            // 
            // TwoButton
            // 
            this.TwoButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TwoButton.BackColor = System.Drawing.Color.DarkKhaki;
            this.TwoButton.FlatAppearance.BorderColor = System.Drawing.Color.Cornsilk;
            this.TwoButton.FlatAppearance.BorderSize = 2;
            this.TwoButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Cornsilk;
            this.TwoButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Cornsilk;
            this.TwoButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.TwoButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TwoButton.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.TwoButton.Location = new System.Drawing.Point(244, 230);
            this.TwoButton.Name = "TwoButton";
            this.TwoButton.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TwoButton.Size = new System.Drawing.Size(50, 50);
            this.TwoButton.TabIndex = 2;
            this.TwoButton.TabStop = false;
            this.TwoButton.Text = "2";
            this.TwoButton.UseVisualStyleBackColor = false;
            this.TwoButton.Click += new System.EventHandler(this.ZeroButton_Click);
            // 
            // OneButton
            // 
            this.OneButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.OneButton.BackColor = System.Drawing.Color.DarkKhaki;
            this.OneButton.FlatAppearance.BorderColor = System.Drawing.Color.Cornsilk;
            this.OneButton.FlatAppearance.BorderSize = 2;
            this.OneButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Cornsilk;
            this.OneButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Cornsilk;
            this.OneButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.OneButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.OneButton.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.OneButton.Location = new System.Drawing.Point(188, 230);
            this.OneButton.Name = "OneButton";
            this.OneButton.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.OneButton.Size = new System.Drawing.Size(50, 50);
            this.OneButton.TabIndex = 1;
            this.OneButton.TabStop = false;
            this.OneButton.Text = "1";
            this.OneButton.UseVisualStyleBackColor = false;
            this.OneButton.Click += new System.EventHandler(this.ZeroButton_Click);
            // 
            // ZeroButton
            // 
            this.ZeroButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ZeroButton.BackColor = System.Drawing.Color.DarkKhaki;
            this.ZeroButton.FlatAppearance.BorderColor = System.Drawing.Color.Cornsilk;
            this.ZeroButton.FlatAppearance.BorderSize = 2;
            this.ZeroButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Cornsilk;
            this.ZeroButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Cornsilk;
            this.ZeroButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ZeroButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ZeroButton.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.ZeroButton.Location = new System.Drawing.Point(244, 295);
            this.ZeroButton.Name = "ZeroButton";
            this.ZeroButton.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ZeroButton.Size = new System.Drawing.Size(50, 50);
            this.ZeroButton.TabIndex = 0;
            this.ZeroButton.TabStop = false;
            this.ZeroButton.Text = "0";
            this.ZeroButton.UseVisualStyleBackColor = false;
            this.ZeroButton.Click += new System.EventHandler(this.ZeroButton_Click);
            // 
            // BackspaceButton
            // 
            this.BackspaceButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.BackspaceButton.BackColor = System.Drawing.Color.DarkKhaki;
            this.BackspaceButton.FlatAppearance.BorderColor = System.Drawing.Color.Cornsilk;
            this.BackspaceButton.FlatAppearance.BorderSize = 2;
            this.BackspaceButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Cornsilk;
            this.BackspaceButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Cornsilk;
            this.BackspaceButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BackspaceButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.BackspaceButton.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.BackspaceButton.Location = new System.Drawing.Point(300, 295);
            this.BackspaceButton.Name = "BackspaceButton";
            this.BackspaceButton.Size = new System.Drawing.Size(106, 50);
            this.BackspaceButton.TabIndex = 7;
            this.BackspaceButton.TabStop = false;
            this.BackspaceButton.Text = "Backspace";
            this.BackspaceButton.UseVisualStyleBackColor = false;
            this.BackspaceButton.Click += new System.EventHandler(this.BackspaceButton_Click);
            // 
            // CancelButton
            // 
            this.CancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CancelButton.BackColor = System.Drawing.Color.DarkKhaki;
            this.CancelButton.FlatAppearance.BorderColor = System.Drawing.Color.Cornsilk;
            this.CancelButton.FlatAppearance.BorderSize = 2;
            this.CancelButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Cornsilk;
            this.CancelButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Cornsilk;
            this.CancelButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CancelButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CancelButton.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.CancelButton.Location = new System.Drawing.Point(188, 294);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(50, 50);
            this.CancelButton.TabIndex = 6;
            this.CancelButton.TabStop = false;
            this.CancelButton.Text = "C";
            this.CancelButton.UseVisualStyleBackColor = false;
            this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // EqualButton
            // 
            this.EqualButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.EqualButton.BackColor = System.Drawing.Color.DarkKhaki;
            this.EqualButton.FlatAppearance.BorderColor = System.Drawing.Color.Cornsilk;
            this.EqualButton.FlatAppearance.BorderSize = 2;
            this.EqualButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Cornsilk;
            this.EqualButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Cornsilk;
            this.EqualButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.EqualButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.EqualButton.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.EqualButton.Location = new System.Drawing.Point(546, 295);
            this.EqualButton.Name = "EqualButton";
            this.EqualButton.Size = new System.Drawing.Size(50, 106);
            this.EqualButton.TabIndex = 0;
            this.EqualButton.TabStop = false;
            this.EqualButton.Text = "=";
            this.EqualButton.UseVisualStyleBackColor = false;
            this.EqualButton.Click += new System.EventHandler(this.EqualButton_Click);
            // 
            // PointButton
            // 
            this.PointButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PointButton.BackColor = System.Drawing.Color.DarkKhaki;
            this.PointButton.FlatAppearance.BorderColor = System.Drawing.Color.Cornsilk;
            this.PointButton.FlatAppearance.BorderSize = 2;
            this.PointButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Cornsilk;
            this.PointButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Cornsilk;
            this.PointButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PointButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.PointButton.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.PointButton.Location = new System.Drawing.Point(546, 230);
            this.PointButton.Name = "PointButton";
            this.PointButton.Size = new System.Drawing.Size(50, 50);
            this.PointButton.TabIndex = 4;
            this.PointButton.TabStop = false;
            this.PointButton.Text = ".";
            this.PointButton.UseVisualStyleBackColor = false;
            this.PointButton.Click += new System.EventHandler(this.PointButton_Click);
            // 
            // DivButton
            // 
            this.DivButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DivButton.BackColor = System.Drawing.Color.DarkKhaki;
            this.DivButton.FlatAppearance.BorderColor = System.Drawing.Color.Cornsilk;
            this.DivButton.FlatAppearance.BorderSize = 2;
            this.DivButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Cornsilk;
            this.DivButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Cornsilk;
            this.DivButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.DivButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.DivButton.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.DivButton.Location = new System.Drawing.Point(546, 165);
            this.DivButton.Name = "DivButton";
            this.DivButton.Size = new System.Drawing.Size(50, 50);
            this.DivButton.TabIndex = 3;
            this.DivButton.TabStop = false;
            this.DivButton.Text = "/";
            this.DivButton.UseVisualStyleBackColor = false;
            this.DivButton.Click += new System.EventHandler(this.AddButton_Click);
            // 
            // MultiplyButton
            // 
            this.MultiplyButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MultiplyButton.BackColor = System.Drawing.Color.DarkKhaki;
            this.MultiplyButton.FlatAppearance.BorderColor = System.Drawing.Color.Cornsilk;
            this.MultiplyButton.FlatAppearance.BorderSize = 2;
            this.MultiplyButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Cornsilk;
            this.MultiplyButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Cornsilk;
            this.MultiplyButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MultiplyButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.MultiplyButton.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.MultiplyButton.Location = new System.Drawing.Point(490, 165);
            this.MultiplyButton.Name = "MultiplyButton";
            this.MultiplyButton.Size = new System.Drawing.Size(50, 50);
            this.MultiplyButton.TabIndex = 2;
            this.MultiplyButton.TabStop = false;
            this.MultiplyButton.Text = "*";
            this.MultiplyButton.UseVisualStyleBackColor = false;
            this.MultiplyButton.Click += new System.EventHandler(this.AddButton_Click);
            // 
            // SubtractButton
            // 
            this.SubtractButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SubtractButton.BackColor = System.Drawing.Color.DarkKhaki;
            this.SubtractButton.FlatAppearance.BorderColor = System.Drawing.Color.Cornsilk;
            this.SubtractButton.FlatAppearance.BorderSize = 2;
            this.SubtractButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Cornsilk;
            this.SubtractButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Cornsilk;
            this.SubtractButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SubtractButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.SubtractButton.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.SubtractButton.Location = new System.Drawing.Point(546, 104);
            this.SubtractButton.Name = "SubtractButton";
            this.SubtractButton.Size = new System.Drawing.Size(50, 50);
            this.SubtractButton.TabIndex = 1;
            this.SubtractButton.TabStop = false;
            this.SubtractButton.Text = "-";
            this.SubtractButton.UseVisualStyleBackColor = false;
            this.SubtractButton.Click += new System.EventHandler(this.AddButton_Click);
            // 
            // AddButton
            // 
            this.AddButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.AddButton.BackColor = System.Drawing.Color.DarkKhaki;
            this.AddButton.FlatAppearance.BorderColor = System.Drawing.Color.Cornsilk;
            this.AddButton.FlatAppearance.BorderSize = 2;
            this.AddButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Cornsilk;
            this.AddButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Cornsilk;
            this.AddButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AddButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AddButton.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.AddButton.Location = new System.Drawing.Point(490, 104);
            this.AddButton.Name = "AddButton";
            this.AddButton.Size = new System.Drawing.Size(50, 50);
            this.AddButton.TabIndex = 0;
            this.AddButton.TabStop = false;
            this.AddButton.Text = "+";
            this.AddButton.UseVisualStyleBackColor = false;
            this.AddButton.Click += new System.EventHandler(this.AddButton_Click);
            // 
            // FactorialButton
            // 
            this.FactorialButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FactorialButton.BackColor = System.Drawing.Color.DarkKhaki;
            this.FactorialButton.FlatAppearance.BorderColor = System.Drawing.Color.Cornsilk;
            this.FactorialButton.FlatAppearance.BorderSize = 2;
            this.FactorialButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Cornsilk;
            this.FactorialButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Cornsilk;
            this.FactorialButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.FactorialButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FactorialButton.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.FactorialButton.Location = new System.Drawing.Point(412, 351);
            this.FactorialButton.Name = "FactorialButton";
            this.FactorialButton.Size = new System.Drawing.Size(50, 50);
            this.FactorialButton.TabIndex = 10;
            this.FactorialButton.TabStop = false;
            this.FactorialButton.Text = "!";
            this.FactorialButton.UseVisualStyleBackColor = false;
            this.FactorialButton.Click += new System.EventHandler(this.ReverseButton_Click);
            // 
            // ReverseButton
            // 
            this.ReverseButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ReverseButton.BackColor = System.Drawing.Color.DarkKhaki;
            this.ReverseButton.FlatAppearance.BorderColor = System.Drawing.Color.Cornsilk;
            this.ReverseButton.FlatAppearance.BorderSize = 2;
            this.ReverseButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Cornsilk;
            this.ReverseButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Cornsilk;
            this.ReverseButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ReverseButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ReverseButton.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.ReverseButton.Location = new System.Drawing.Point(188, 351);
            this.ReverseButton.Name = "ReverseButton";
            this.ReverseButton.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ReverseButton.Size = new System.Drawing.Size(50, 50);
            this.ReverseButton.TabIndex = 9;
            this.ReverseButton.TabStop = false;
            this.ReverseButton.Text = "1/x";
            this.ReverseButton.UseVisualStyleBackColor = false;
            this.ReverseButton.Click += new System.EventHandler(this.ReverseButton_Click);
            // 
            // SignumButton
            // 
            this.SignumButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SignumButton.BackColor = System.Drawing.Color.DarkKhaki;
            this.SignumButton.FlatAppearance.BorderColor = System.Drawing.Color.Cornsilk;
            this.SignumButton.FlatAppearance.BorderSize = 2;
            this.SignumButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Cornsilk;
            this.SignumButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Cornsilk;
            this.SignumButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SignumButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.SignumButton.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.SignumButton.Location = new System.Drawing.Point(490, 230);
            this.SignumButton.Name = "SignumButton";
            this.SignumButton.Size = new System.Drawing.Size(50, 50);
            this.SignumButton.TabIndex = 8;
            this.SignumButton.TabStop = false;
            this.SignumButton.Text = "+/-";
            this.SignumButton.UseVisualStyleBackColor = false;
            this.SignumButton.Click += new System.EventHandler(this.SignumButton_Click);
            // 
            // expXButton
            // 
            this.expXButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.expXButton.BackColor = System.Drawing.Color.DarkKhaki;
            this.expXButton.FlatAppearance.BorderColor = System.Drawing.Color.Cornsilk;
            this.expXButton.FlatAppearance.BorderSize = 2;
            this.expXButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Cornsilk;
            this.expXButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Cornsilk;
            this.expXButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.expXButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.expXButton.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.expXButton.Location = new System.Drawing.Point(412, 295);
            this.expXButton.Name = "expXButton";
            this.expXButton.Size = new System.Drawing.Size(50, 50);
            this.expXButton.TabIndex = 13;
            this.expXButton.TabStop = false;
            this.expXButton.Text = "e^x";
            this.expXButton.UseVisualStyleBackColor = false;
            this.expXButton.Click += new System.EventHandler(this.ReverseButton_Click);
            // 
            // SqrButton
            // 
            this.SqrButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SqrButton.BackColor = System.Drawing.Color.DarkKhaki;
            this.SqrButton.FlatAppearance.BorderColor = System.Drawing.Color.Cornsilk;
            this.SqrButton.FlatAppearance.BorderSize = 2;
            this.SqrButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Cornsilk;
            this.SqrButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Cornsilk;
            this.SqrButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SqrButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.SqrButton.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.SqrButton.Location = new System.Drawing.Point(244, 351);
            this.SqrButton.Name = "SqrButton";
            this.SqrButton.Size = new System.Drawing.Size(50, 50);
            this.SqrButton.TabIndex = 12;
            this.SqrButton.TabStop = false;
            this.SqrButton.Text = "x^2";
            this.SqrButton.UseVisualStyleBackColor = false;
            this.SqrButton.Click += new System.EventHandler(this.ReverseButton_Click);
            // 
            // SqrtButton
            // 
            this.SqrtButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SqrtButton.BackColor = System.Drawing.Color.DarkKhaki;
            this.SqrtButton.FlatAppearance.BorderColor = System.Drawing.Color.Cornsilk;
            this.SqrtButton.FlatAppearance.BorderSize = 2;
            this.SqrtButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Cornsilk;
            this.SqrtButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Cornsilk;
            this.SqrtButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SqrtButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.SqrtButton.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.SqrtButton.Location = new System.Drawing.Point(300, 351);
            this.SqrtButton.Name = "SqrtButton";
            this.SqrtButton.Size = new System.Drawing.Size(50, 50);
            this.SqrtButton.TabIndex = 11;
            this.SqrtButton.TabStop = false;
            this.SqrtButton.Text = "sqrt";
            this.SqrtButton.UseVisualStyleBackColor = false;
            this.SqrtButton.Click += new System.EventHandler(this.ReverseButton_Click);
            // 
            // HexButton
            // 
            this.HexButton.AutoSize = true;
            this.HexButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.HexButton.Location = new System.Drawing.Point(5, 142);
            this.HexButton.Name = "HexButton";
            this.HexButton.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.HexButton.Size = new System.Drawing.Size(146, 19);
            this.HexButton.TabIndex = 4;
            this.HexButton.Text = "Шестнадцатеричная";
            this.HexButton.UseVisualStyleBackColor = true;
            this.HexButton.CheckedChanged += new System.EventHandler(this.HexButton_CheckedChanged);
            // 
            // DecButton
            // 
            this.DecButton.AutoSize = true;
            this.DecButton.Checked = true;
            this.DecButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.DecButton.Location = new System.Drawing.Point(5, 104);
            this.DecButton.Name = "DecButton";
            this.DecButton.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.DecButton.Size = new System.Drawing.Size(95, 19);
            this.DecButton.TabIndex = 3;
            this.DecButton.TabStop = true;
            this.DecButton.Text = "Десятичная";
            this.DecButton.UseVisualStyleBackColor = true;
            this.DecButton.CheckedChanged += new System.EventHandler(this.DecButton_CheckedChanged);
            // 
            // OctButton
            // 
            this.OctButton.AutoSize = true;
            this.OctButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.OctButton.Location = new System.Drawing.Point(5, 66);
            this.OctButton.Name = "OctButton";
            this.OctButton.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.OctButton.Size = new System.Drawing.Size(110, 19);
            this.OctButton.TabIndex = 2;
            this.OctButton.Text = "Восьмеричная";
            this.OctButton.UseVisualStyleBackColor = true;
            this.OctButton.CheckedChanged += new System.EventHandler(this.OctButton_CheckedChanged);
            // 
            // BinButton
            // 
            this.BinButton.AutoSize = true;
            this.BinButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.BinButton.Location = new System.Drawing.Point(6, 29);
            this.BinButton.Name = "BinButton";
            this.BinButton.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.BinButton.Size = new System.Drawing.Size(82, 19);
            this.BinButton.TabIndex = 1;
            this.BinButton.Text = "Двоичная";
            this.BinButton.UseVisualStyleBackColor = true;
            this.BinButton.CheckedChanged += new System.EventHandler(this.BinButton_CheckedChanged);
            // 
            // HexEButton
            // 
            this.HexEButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.HexEButton.BackColor = System.Drawing.Color.DarkKhaki;
            this.HexEButton.Enabled = false;
            this.HexEButton.FlatAppearance.BorderColor = System.Drawing.Color.Cornsilk;
            this.HexEButton.FlatAppearance.BorderSize = 2;
            this.HexEButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Cornsilk;
            this.HexEButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Cornsilk;
            this.HexEButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.HexEButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.HexEButton.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.HexEButton.Location = new System.Drawing.Point(412, 165);
            this.HexEButton.Name = "HexEButton";
            this.HexEButton.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.HexEButton.Size = new System.Drawing.Size(50, 50);
            this.HexEButton.TabIndex = 14;
            this.HexEButton.TabStop = false;
            this.HexEButton.Text = "e";
            this.HexEButton.UseVisualStyleBackColor = false;
            this.HexEButton.Click += new System.EventHandler(this.ZeroButton_Click);
            // 
            // HexDButton
            // 
            this.HexDButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.HexDButton.BackColor = System.Drawing.Color.DarkKhaki;
            this.HexDButton.Enabled = false;
            this.HexDButton.FlatAppearance.BorderColor = System.Drawing.Color.Cornsilk;
            this.HexDButton.FlatAppearance.BorderSize = 2;
            this.HexDButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Cornsilk;
            this.HexDButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Cornsilk;
            this.HexDButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.HexDButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.HexDButton.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.HexDButton.Location = new System.Drawing.Point(412, 104);
            this.HexDButton.Name = "HexDButton";
            this.HexDButton.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.HexDButton.Size = new System.Drawing.Size(50, 50);
            this.HexDButton.TabIndex = 13;
            this.HexDButton.TabStop = false;
            this.HexDButton.Text = "d";
            this.HexDButton.UseVisualStyleBackColor = false;
            this.HexDButton.Click += new System.EventHandler(this.ZeroButton_Click);
            // 
            // HexCButton
            // 
            this.HexCButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.HexCButton.BackColor = System.Drawing.Color.DarkKhaki;
            this.HexCButton.Enabled = false;
            this.HexCButton.FlatAppearance.BorderColor = System.Drawing.Color.Cornsilk;
            this.HexCButton.FlatAppearance.BorderSize = 2;
            this.HexCButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Cornsilk;
            this.HexCButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Cornsilk;
            this.HexCButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.HexCButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.HexCButton.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.HexCButton.Location = new System.Drawing.Point(356, 230);
            this.HexCButton.Name = "HexCButton";
            this.HexCButton.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.HexCButton.Size = new System.Drawing.Size(50, 50);
            this.HexCButton.TabIndex = 12;
            this.HexCButton.TabStop = false;
            this.HexCButton.Text = "c";
            this.HexCButton.UseVisualStyleBackColor = false;
            this.HexCButton.Click += new System.EventHandler(this.ZeroButton_Click);
            // 
            // HexBButton
            // 
            this.HexBButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.HexBButton.BackColor = System.Drawing.Color.DarkKhaki;
            this.HexBButton.Enabled = false;
            this.HexBButton.FlatAppearance.BorderColor = System.Drawing.Color.Cornsilk;
            this.HexBButton.FlatAppearance.BorderSize = 2;
            this.HexBButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Cornsilk;
            this.HexBButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Cornsilk;
            this.HexBButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.HexBButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.HexBButton.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.HexBButton.Location = new System.Drawing.Point(356, 165);
            this.HexBButton.Name = "HexBButton";
            this.HexBButton.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.HexBButton.Size = new System.Drawing.Size(50, 50);
            this.HexBButton.TabIndex = 11;
            this.HexBButton.TabStop = false;
            this.HexBButton.Text = "b";
            this.HexBButton.UseVisualStyleBackColor = false;
            this.HexBButton.Click += new System.EventHandler(this.ZeroButton_Click);
            // 
            // HexAButton
            // 
            this.HexAButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.HexAButton.BackColor = System.Drawing.Color.DarkKhaki;
            this.HexAButton.Enabled = false;
            this.HexAButton.FlatAppearance.BorderColor = System.Drawing.Color.Cornsilk;
            this.HexAButton.FlatAppearance.BorderSize = 2;
            this.HexAButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Cornsilk;
            this.HexAButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Cornsilk;
            this.HexAButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.HexAButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.HexAButton.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.HexAButton.Location = new System.Drawing.Point(356, 104);
            this.HexAButton.Name = "HexAButton";
            this.HexAButton.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.HexAButton.Size = new System.Drawing.Size(50, 50);
            this.HexAButton.TabIndex = 10;
            this.HexAButton.TabStop = false;
            this.HexAButton.Text = "a";
            this.HexAButton.UseVisualStyleBackColor = false;
            this.HexAButton.Click += new System.EventHandler(this.ZeroButton_Click);
            // 
            // HexFButton
            // 
            this.HexFButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.HexFButton.BackColor = System.Drawing.Color.DarkKhaki;
            this.HexFButton.Enabled = false;
            this.HexFButton.FlatAppearance.BorderColor = System.Drawing.Color.Cornsilk;
            this.HexFButton.FlatAppearance.BorderSize = 2;
            this.HexFButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Cornsilk;
            this.HexFButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Cornsilk;
            this.HexFButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.HexFButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.HexFButton.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.HexFButton.Location = new System.Drawing.Point(412, 230);
            this.HexFButton.Name = "HexFButton";
            this.HexFButton.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.HexFButton.Size = new System.Drawing.Size(50, 50);
            this.HexFButton.TabIndex = 10;
            this.HexFButton.TabStop = false;
            this.HexFButton.Text = "f";
            this.HexFButton.UseVisualStyleBackColor = false;
            this.HexFButton.Click += new System.EventHandler(this.ZeroButton_Click);
            // 
            // andButton
            // 
            this.andButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.andButton.BackColor = System.Drawing.Color.DarkKhaki;
            this.andButton.Enabled = false;
            this.andButton.FlatAppearance.BorderColor = System.Drawing.Color.Cornsilk;
            this.andButton.FlatAppearance.BorderSize = 2;
            this.andButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Cornsilk;
            this.andButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Cornsilk;
            this.andButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.andButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.andButton.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.andButton.Location = new System.Drawing.Point(12, 294);
            this.andButton.Name = "andButton";
            this.andButton.Size = new System.Drawing.Size(50, 50);
            this.andButton.TabIndex = 15;
            this.andButton.TabStop = false;
            this.andButton.Text = "and";
            this.andButton.UseVisualStyleBackColor = false;
            this.andButton.Click += new System.EventHandler(this.AddButton_Click);
            // 
            // orButton
            // 
            this.orButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.orButton.BackColor = System.Drawing.Color.DarkKhaki;
            this.orButton.Enabled = false;
            this.orButton.FlatAppearance.BorderColor = System.Drawing.Color.Cornsilk;
            this.orButton.FlatAppearance.BorderSize = 2;
            this.orButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Cornsilk;
            this.orButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Cornsilk;
            this.orButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.orButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.orButton.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.orButton.Location = new System.Drawing.Point(67, 294);
            this.orButton.Name = "orButton";
            this.orButton.Size = new System.Drawing.Size(50, 50);
            this.orButton.TabIndex = 16;
            this.orButton.TabStop = false;
            this.orButton.Text = "or";
            this.orButton.UseVisualStyleBackColor = false;
            this.orButton.Click += new System.EventHandler(this.AddButton_Click);
            // 
            // notButton
            // 
            this.notButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.notButton.BackColor = System.Drawing.Color.DarkKhaki;
            this.notButton.Enabled = false;
            this.notButton.FlatAppearance.BorderColor = System.Drawing.Color.Cornsilk;
            this.notButton.FlatAppearance.BorderSize = 2;
            this.notButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Cornsilk;
            this.notButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Cornsilk;
            this.notButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.notButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.notButton.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.notButton.Location = new System.Drawing.Point(121, 294);
            this.notButton.Name = "notButton";
            this.notButton.Size = new System.Drawing.Size(50, 50);
            this.notButton.TabIndex = 17;
            this.notButton.TabStop = false;
            this.notButton.Text = "not";
            this.notButton.UseVisualStyleBackColor = false;
            this.notButton.Click += new System.EventHandler(this.ReverseButton_Click);
            // 
            // PiButton
            // 
            this.PiButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PiButton.BackColor = System.Drawing.Color.DarkKhaki;
            this.PiButton.FlatAppearance.BorderColor = System.Drawing.Color.Cornsilk;
            this.PiButton.FlatAppearance.BorderSize = 2;
            this.PiButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Cornsilk;
            this.PiButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Cornsilk;
            this.PiButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PiButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.PiButton.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.PiButton.Location = new System.Drawing.Point(490, 351);
            this.PiButton.Name = "PiButton";
            this.PiButton.Size = new System.Drawing.Size(50, 50);
            this.PiButton.TabIndex = 18;
            this.PiButton.TabStop = false;
            this.PiButton.Text = "Pi";
            this.PiButton.UseVisualStyleBackColor = false;
            this.PiButton.Click += new System.EventHandler(this.ExpButton_Click);
            // 
            // button13
            // 
            this.button13.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button13.BackColor = System.Drawing.Color.DarkKhaki;
            this.button13.FlatAppearance.BorderColor = System.Drawing.Color.Cornsilk;
            this.button13.FlatAppearance.BorderSize = 2;
            this.button13.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Cornsilk;
            this.button13.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Cornsilk;
            this.button13.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button13.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button13.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.button13.Location = new System.Drawing.Point(356, 351);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(50, 50);
            this.button13.TabIndex = 19;
            this.button13.TabStop = false;
            this.button13.Text = "x^n";
            this.button13.UseVisualStyleBackColor = false;
            this.button13.Click += new System.EventHandler(this.AddButton_Click);
            // 
            // sinButton
            // 
            this.sinButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sinButton.BackColor = System.Drawing.Color.DarkKhaki;
            this.sinButton.FlatAppearance.BorderColor = System.Drawing.Color.Cornsilk;
            this.sinButton.FlatAppearance.BorderSize = 2;
            this.sinButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Cornsilk;
            this.sinButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Cornsilk;
            this.sinButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.sinButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.sinButton.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.sinButton.Location = new System.Drawing.Point(13, 351);
            this.sinButton.Name = "sinButton";
            this.sinButton.Size = new System.Drawing.Size(50, 50);
            this.sinButton.TabIndex = 20;
            this.sinButton.TabStop = false;
            this.sinButton.Text = "sin";
            this.sinButton.UseVisualStyleBackColor = false;
            this.sinButton.Click += new System.EventHandler(this.ReverseButton_Click);
            // 
            // expButton
            // 
            this.expButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.expButton.BackColor = System.Drawing.Color.DarkKhaki;
            this.expButton.FlatAppearance.BorderColor = System.Drawing.Color.Cornsilk;
            this.expButton.FlatAppearance.BorderSize = 2;
            this.expButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Cornsilk;
            this.expButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Cornsilk;
            this.expButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.expButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.expButton.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.expButton.Location = new System.Drawing.Point(490, 294);
            this.expButton.Name = "expButton";
            this.expButton.Size = new System.Drawing.Size(50, 50);
            this.expButton.TabIndex = 21;
            this.expButton.TabStop = false;
            this.expButton.Text = "e";
            this.expButton.UseVisualStyleBackColor = false;
            this.expButton.Click += new System.EventHandler(this.ExpButton_Click);
            // 
            // tgButton
            // 
            this.tgButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tgButton.BackColor = System.Drawing.Color.DarkKhaki;
            this.tgButton.FlatAppearance.BorderColor = System.Drawing.Color.Cornsilk;
            this.tgButton.FlatAppearance.BorderSize = 2;
            this.tgButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Cornsilk;
            this.tgButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Cornsilk;
            this.tgButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.tgButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tgButton.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.tgButton.Location = new System.Drawing.Point(121, 351);
            this.tgButton.Name = "tgButton";
            this.tgButton.Size = new System.Drawing.Size(50, 50);
            this.tgButton.TabIndex = 22;
            this.tgButton.TabStop = false;
            this.tgButton.Text = "tg";
            this.tgButton.UseVisualStyleBackColor = false;
            this.tgButton.Click += new System.EventHandler(this.ReverseButton_Click);
            // 
            // cosButton
            // 
            this.cosButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cosButton.BackColor = System.Drawing.Color.DarkKhaki;
            this.cosButton.FlatAppearance.BorderColor = System.Drawing.Color.Cornsilk;
            this.cosButton.FlatAppearance.BorderSize = 2;
            this.cosButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Cornsilk;
            this.cosButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Cornsilk;
            this.cosButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cosButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cosButton.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.cosButton.Location = new System.Drawing.Point(67, 351);
            this.cosButton.Name = "cosButton";
            this.cosButton.Size = new System.Drawing.Size(50, 50);
            this.cosButton.TabIndex = 23;
            this.cosButton.TabStop = false;
            this.cosButton.Text = "cos";
            this.cosButton.UseVisualStyleBackColor = false;
            this.cosButton.Click += new System.EventHandler(this.ReverseButton_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.BinButton);
            this.groupBox1.Controls.Add(this.OctButton);
            this.groupBox1.Controls.Add(this.HexButton);
            this.groupBox1.Controls.Add(this.DecButton);
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.groupBox1.Location = new System.Drawing.Point(13, 97);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(158, 183);
            this.groupBox1.TabIndex = 24;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Системы счисления";
            // 
            // NumberTextBox
            // 
            this.NumberTextBox.BackColor = System.Drawing.Color.Olive;
            this.NumberTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.NumberTextBox.Cursor = System.Windows.Forms.Cursors.Default;
            this.NumberTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 38.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.NumberTextBox.ForeColor = System.Drawing.Color.Cornsilk;
            this.NumberTextBox.Location = new System.Drawing.Point(13, 18);
            this.NumberTextBox.MaxLength = 1;
            this.NumberTextBox.Name = "NumberTextBox";
            this.NumberTextBox.ReadOnly = true;
            this.NumberTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.NumberTextBox.Size = new System.Drawing.Size(577, 58);
            this.NumberTextBox.TabIndex = 26;
            this.NumberTextBox.TabStop = false;
            this.NumberTextBox.Text = "0";
            // 
            // Calculator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Olive;
            this.ClientSize = new System.Drawing.Size(605, 406);
            this.Controls.Add(this.SevenButton);
            this.Controls.Add(this.NumberTextBox);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.cosButton);
            this.Controls.Add(this.tgButton);
            this.Controls.Add(this.expButton);
            this.Controls.Add(this.sinButton);
            this.Controls.Add(this.button13);
            this.Controls.Add(this.PiButton);
            this.Controls.Add(this.notButton);
            this.Controls.Add(this.orButton);
            this.Controls.Add(this.andButton);
            this.Controls.Add(this.SqrButton);
            this.Controls.Add(this.expXButton);
            this.Controls.Add(this.ZeroButton);
            this.Controls.Add(this.OneButton);
            this.Controls.Add(this.FactorialButton);
            this.Controls.Add(this.SqrtButton);
            this.Controls.Add(this.ReverseButton);
            this.Controls.Add(this.TwoButton);
            this.Controls.Add(this.DivButton);
            this.Controls.Add(this.PointButton);
            this.Controls.Add(this.SignumButton);
            this.Controls.Add(this.ThreeButton);
            this.Controls.Add(this.FourButton);
            this.Controls.Add(this.NineButton);
            this.Controls.Add(this.BackspaceButton);
            this.Controls.Add(this.HexFButton);
            this.Controls.Add(this.CancelButton);
            this.Controls.Add(this.EightButton);
            this.Controls.Add(this.EqualButton);
            this.Controls.Add(this.HexEButton);
            this.Controls.Add(this.MultiplyButton);
            this.Controls.Add(this.SixButton);
            this.Controls.Add(this.SubtractButton);
            this.Controls.Add(this.HexDButton);
            this.Controls.Add(this.AddButton);
            this.Controls.Add(this.FiveButton);
            this.Controls.Add(this.HexAButton);
            this.Controls.Add(this.HexCButton);
            this.Controls.Add(this.HexBButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "Calculator";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Калькулятор";
            this.Click += new System.EventHandler(this.ZeroButton_Click);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Calculator_KeyDown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Calculator_KeyPress);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button NineButton;
        private System.Windows.Forms.Button EightButton;
        private System.Windows.Forms.Button SevenButton;
        private System.Windows.Forms.Button SixButton;
        private System.Windows.Forms.Button FiveButton;
        private System.Windows.Forms.Button FourButton;
        private System.Windows.Forms.Button ThreeButton;
        private System.Windows.Forms.Button TwoButton;
        private System.Windows.Forms.Button OneButton;
        private System.Windows.Forms.Button ZeroButton;
        private System.Windows.Forms.Button BackspaceButton;
        private System.Windows.Forms.Button CancelButton;
        private System.Windows.Forms.Button EqualButton;
        private System.Windows.Forms.Button PointButton;
        private System.Windows.Forms.Button DivButton;
        private System.Windows.Forms.Button MultiplyButton;
        private System.Windows.Forms.Button SubtractButton;
        private System.Windows.Forms.Button AddButton;
        private System.Windows.Forms.Button FactorialButton;
        private System.Windows.Forms.Button ReverseButton;
        private System.Windows.Forms.Button SignumButton;
        private System.Windows.Forms.Button expXButton;
        private System.Windows.Forms.Button SqrButton;
        private System.Windows.Forms.Button SqrtButton;
        private System.Windows.Forms.RadioButton HexButton;
        private System.Windows.Forms.RadioButton DecButton;
        private System.Windows.Forms.RadioButton OctButton;
        private System.Windows.Forms.RadioButton BinButton;
        private System.Windows.Forms.Button HexEButton;
        private System.Windows.Forms.Button HexDButton;
        private System.Windows.Forms.Button HexCButton;
        private System.Windows.Forms.Button HexBButton;
        private System.Windows.Forms.Button HexAButton;
        private System.Windows.Forms.Button HexFButton;
        private System.Windows.Forms.Button andButton;
        private System.Windows.Forms.Button orButton;
        private System.Windows.Forms.Button notButton;
        private System.Windows.Forms.Button PiButton;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button sinButton;
        private System.Windows.Forms.Button expButton;
        private System.Windows.Forms.Button tgButton;
        private System.Windows.Forms.Button cosButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox NumberTextBox;
    }
}


