﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Калькулятор
{
    public partial class Calculator : Form
    {
        public Calculator()
        {
            InitializeComponent();
        }

        bool checkPerformOperation = false;
        double firstNumber, secondNumber;
        string sign;
        private void ZeroButton_Click(object sender, EventArgs e)
        {
            if (checkPerformOperation)
            {
                NumberTextBox.Text = "";
                checkPerformOperation = false;
            }
            if (NumberTextBox.Text == "0") NumberTextBox.Text = "";
            if (NumberTextBox.Text.Length < 9)
                try
                {
                    NumberTextBox.Text += (sender as Button).Text;
                }
                catch
                {
                    NumberTextBox.Text = "0";
                }
            EqualButton.Select();
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            sign = (sender as Button).Text;
            firstNumber = double.Parse(NumberTextBox.Text);
            NumberTextBox.Text = "0";
            EqualButton.Select();
        }

        private void EqualButton_Click(object sender, EventArgs e)
        {
            Equal();
        }

        private void ReverseButton_Click(object sender, EventArgs e)
        {
            checkPerformOperation = true;
            firstNumber = double.Parse(NumberTextBox.Text);
            sign = (sender as Button).Text;
            var unaryOperation = new UnaryOperation(firstNumber);
            NumberTextBox.Text = unaryOperation.PerformOperation(sign).ToString();
        }

        private void ExpButton_Click(object sender, EventArgs e)
        {
            checkPerformOperation = true;
            if ((sender as Button).Text == "e") NumberTextBox.Text = Math.E.ToString();
            else NumberTextBox.Text = Math.PI.ToString();
            EqualButton.Select();
        }

        private void SignumButton_Click(object sender, EventArgs e)
        {
            if (NumberTextBox.Text != "0")
            {
                firstNumber = -firstNumber;
                if (NumberTextBox.Text[0] != '-') NumberTextBox.Text = NumberTextBox.Text.Insert(0, "-");
                else NumberTextBox.Text = NumberTextBox.Text.Remove(0, 1);
            }
            EqualButton.Select();
        }

        private void PointButton_Click(object sender, EventArgs e)
        {
            Point();
            EqualButton.Select();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            NumberTextBox.Text = "0";
            EqualButton.Select();
        }

        private void BackspaceButton_Click(object sender, EventArgs e)
        {
            Back();
            EqualButton.Select();
        }

        int begBasis = 10;
        private void BinButton_CheckedChanged(object sender, EventArgs e)
        {
            checkPerformOperation = true;
            ButtonEnabled(false);
            andButton.Enabled = orButton.Enabled = notButton.Enabled = true;
            var translator = new Translator(begBasis, 2);
            NumberTextBox.Text = translator.Translate(NumberTextBox.Text);
            begBasis = 2;
        }

        private void OctButton_CheckedChanged(object sender, EventArgs e)
        {
            checkPerformOperation = true;
            ButtonEnabled(false);
            TwoButton.Enabled = ThreeButton.Enabled = FourButton.Enabled = FiveButton.Enabled = SixButton.Enabled = SevenButton.Enabled = true;
            var translator = new Translator(begBasis, 8);
            NumberTextBox.Text = translator.Translate(NumberTextBox.Text);
            begBasis = 8;
        }

        private void HexButton_CheckedChanged(object sender, EventArgs e)
        {
            checkPerformOperation = true;
            ButtonEnabled(false);
            TwoButton.Enabled = ThreeButton.Enabled = FourButton.Enabled = FiveButton.Enabled = SixButton.Enabled = SevenButton.Enabled =
                EightButton.Enabled = NineButton.Enabled = HexAButton.Enabled = HexBButton.Enabled = HexCButton.Enabled = HexDButton.Enabled = HexEButton.Enabled = HexFButton.Enabled = true;
            var translator = new Translator(begBasis, 16);
            NumberTextBox.Text = translator.Translate(NumberTextBox.Text);
            begBasis = 16;
        }

        private void DecButton_CheckedChanged(object sender, EventArgs e)
        {
            checkPerformOperation = true;
            ButtonEnabled(true);
            HexAButton.Enabled = HexBButton.Enabled = HexCButton.Enabled = HexDButton.Enabled = HexEButton.Enabled = HexFButton.Enabled = false;
            andButton.Enabled = orButton.Enabled = notButton.Enabled = false;
            var translator = new Translator(begBasis, 10);
            NumberTextBox.Text = translator.Translate(NumberTextBox.Text);
            begBasis = 10;
        }

        private void Calculator_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetterOrDigit(e.KeyChar))
            {
                if ((BinButton.Checked && (e.KeyChar == '0' || e.KeyChar == '1')) || 
                    (OctButton.Checked && (e.KeyChar >= '0' && e.KeyChar <= '7')) || 
                    (HexButton.Checked && ((e.KeyChar >= '0' && e.KeyChar <= '9') || (e.KeyChar >= 'a' && e.KeyChar <= 'f') || (e.KeyChar >= 'A' && e.KeyChar <= 'F'))) || 
                    (DecButton.Checked && (e.KeyChar >= '0' && e.KeyChar <= '9')))
                {
                    if (checkPerformOperation)
                    {
                        NumberTextBox.Text = "";
                        checkPerformOperation = false;
                    }
                    if (NumberTextBox.Text == "0") NumberTextBox.Text = "";
                    if (NumberTextBox.Text.Length < 9)
                        NumberTextBox.Text += e.KeyChar;
                }
            } 
            if (e.KeyChar == '+' || e.KeyChar == '-' || e.KeyChar == '*' || e.KeyChar == '/')
            {
                sign = e.KeyChar.ToString();
                double.TryParse(NumberTextBox.Text, out firstNumber);
                NumberTextBox.Text = "0";
            }

        }

        private void Calculator_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Decimal) Point();
            else if (e.KeyCode == Keys.Back) Back();
            else if (e.KeyCode == Keys.Enter) Equal();
        }

        private void Back()
        {
            try
            {
                if (double.IsNaN(double.Parse(NumberTextBox.Text))) NumberTextBox.Text = "0";
            }
            catch { }
            NumberTextBox.Text = NumberTextBox.Text.Remove(NumberTextBox.Text.Length - 1, 1);
            if (NumberTextBox.Text == "" || NumberTextBox.Text == "-") NumberTextBox.Text = "0";
        }

        private void Point()
        {
            if (checkPerformOperation) NumberTextBox.Text = "0";
            checkPerformOperation = false;
            if (!NumberTextBox.Text.Contains(','))
                NumberTextBox.Text += ",";
        }

        private void Equal()
        {
            if (checkPerformOperation) NumberTextBox.Text = "0";
            else
            {
                checkPerformOperation = true;
                secondNumber = double.Parse(NumberTextBox.Text);
                var binaryOperation = new BinaryOperation(firstNumber, secondNumber);
                NumberTextBox.Text = binaryOperation.PerformOperation(sign).ToString();
            }
        }

        private void ButtonEnabled(bool i)
        {
            TwoButton.Enabled = ThreeButton.Enabled = FourButton.Enabled = FiveButton.Enabled = SixButton.Enabled = SevenButton.Enabled = EightButton.Enabled = NineButton.Enabled = i;
            HexAButton.Enabled = HexBButton.Enabled = HexCButton.Enabled = HexDButton.Enabled = HexEButton.Enabled = HexFButton.Enabled = i;
            AddButton.Enabled = DivButton.Enabled = MultiplyButton.Enabled = SubtractButton.Enabled = SignumButton.Enabled = PointButton.Enabled = i;
            andButton.Enabled = orButton.Enabled = notButton.Enabled = sinButton.Enabled = cosButton.Enabled = tgButton.Enabled = ReverseButton.Enabled = SqrButton.Enabled = SqrtButton.Enabled = 
                FactorialButton.Enabled = expButton.Enabled = PiButton.Enabled = expXButton.Enabled = button13.Enabled = i;
        }
    }
}
