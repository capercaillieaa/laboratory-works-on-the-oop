﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Калькулятор
{
    class Translator
    {
        readonly int begBasis, endBasis;
        public Translator(int beg, int end)
        {
            begBasis = beg;
            endBasis = end;
        }

        public string Translate (string str)
        {
            try
            {
                if (double.IsInfinity(double.Parse(str)) || double.IsNaN(double.Parse(str))) return "0";
            }
            catch { }
            int i = TranslateToDec(str);
            return Convert.ToString(i, endBasis);
        }

        private int TranslateToDec(string str)
        {
            int result = 0;
            int value;
            int deg = 0;
            for (int i = str.Length - 1; i >=0; i--)
            {
                if (begBasis == 16) value = HexNumberSystem(str[i]);
                else value = int.Parse(str[i].ToString());
                result += value * (int)Math.Pow(begBasis, deg);
                deg++;
            }
            return result;
        }

        private int HexNumberSystem(char value)
        {
            if (value == 'a') return 10;
            if (value == 'b') return 11;
            if (value == 'c') return 12;
            if (value == 'd') return 13;
            if (value == 'e') return 14;
            if (value == 'f') return 15;
            else return int.Parse(value.ToString());
        }
    }
}
