﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Домашняя_библиотека
{
    class Program
    {
        static void Main(string[] args)
        {
            var library = new List<Book>();
            using (StreamReader books = new StreamReader("Книги.txt"))
            {
                while (!books.EndOfStream)
                {
                    var str = new string[7];
                    str = books.ReadLine().Split('\t');
                    try
                    {
                        str[5] = str[5].Remove(str[5].Length - 2, 2);
                        if (str[5] == "-") str[5] = "0";
                        str[6] = str[6].Remove(str[6].Length - 4, 4);
                        if (str[6] == "-") str[6] = "0";
                        library.Add(new Book(int.Parse(str[0]), str[1]) { AuthorSurname = str[2], AuthorName = str[3], AuthorPatronymic = str[4], Year = int.Parse(str[5]), PageNumber = int.Parse(str[6]) });
                    }
                    catch { }
                }
            }
            var homeLibrary = new HomeLibrary(library);

            bool checkExit = false;
            while (!checkExit)
            {
                Console.Clear();
                Console.Write("[1] - Добавить книгу\n[2] - Удалить книгу\n[3] - Сортировать книги\n[4] - Поиск книги\n[5] - Вывести список книг\n[6] - Выход из программы\n");
                int startSwitcher;
                int.TryParse(Console.ReadLine(), out startSwitcher);
                if (startSwitcher <= 0)
                {
                    Console.WriteLine("Введено неверное значение. Попробуйте еще раз!");
                    Console.ReadKey();
                    continue;
                }
                switch (startSwitcher)
                {
                    case 1:
                        {
                            Console.Clear();
                            Console.Write("(*) - поля, обязательные для заполнения\nКод книги(*): ");
                            int code;
                            int newCode;
                            int.TryParse(Console.ReadLine(), out code);
                            if (code <= 0)
                            {
                                Console.WriteLine("Введены некорректные данные (код должен быть неотрицательным числом)");
                                Console.ReadKey();
                                goto case 1;
                            }
                            if (homeLibrary.CheckCode(new Book() { Code = code }))
                            {
                                Console.WriteLine("Книга с таким кодом уже существует!");
                                inputNewCode :
                                Console.WriteLine("[1] - Ввести новый код\n[2] - Выбрать код автоматически");
                                int.TryParse(Console.ReadLine(), out newCode);
                                if (newCode <= 0)
                                {
                                    Console.WriteLine("Введено неверное значение. Попробуйте еще раз!");
                                    Console.ReadKey();
                                    goto inputNewCode;
                                }
                                if (newCode == 1)
                                    goto case 1;
                                if (newCode == 2)
                                {
                                    code = homeLibrary.CreateNewCode();
                                    Console.Write("Название книги(*): ");
                                }
                            }
                            else Console.Write("Название книги(*): ");
                            string bookName = Console.ReadLine();
                            while (bookName == "") 
                            {
                                Console.WriteLine("Название не может быть пустым полем!");
                                Console.Write("Название книги(*): ");
                                bookName = Console.ReadLine();
                            } 
                            Console.Write("Фамилия автора: ");
                            string aSurname = Console.ReadLine();
                            Console.Write("Имя автора: ");
                            string aName = Console.ReadLine();
                            Console.Write("Отчество автора: ");
                            string aPatronumic = Console.ReadLine();
                            Console.Write("Год издания: ");
                            int year;
                            int.TryParse(Console.ReadLine(), out year);
                            while (year < 0 || year > DateTime.Today.Year)
                            {
                                Console.WriteLine("Год не может быть отрицательным и больше текущего!");
                                Console.Write("Год издания: ");
                                int.TryParse(Console.ReadLine(), out year);
                            }
                            Console.Write("Количество страниц: ");
                            int page;
                            int.TryParse(Console.ReadLine(), out page);
                            while (page < 0 )
                            {
                                Console.WriteLine("Количество страниц дожно быть положительным числом");
                                Console.Write("Количество страниц: ");
                                int.TryParse(Console.ReadLine(), out page);
                            }
                            var newBook = new Book(code, bookName) { AuthorSurname = aSurname, AuthorName = aName, AuthorPatronymic = aPatronumic, Year = year, PageNumber = page};
                            homeLibrary.Add(newBook);
                            break;
                        }
                    case 2:
                        {
                            Console.Clear();
                            Console.WriteLine("[1] - Удалить конкретную книгу\n[2] - Удалить книги определенного автора\n[3] - Удалить книги определенного года издания");
                            int removeSwitcher;
                            int.TryParse(Console.ReadLine(), out removeSwitcher);
                            if (removeSwitcher == 0)
                            {
                                Console.WriteLine("Введено некорректное значение. Попробуйте еще раз!");
                                Console.ReadKey();
                                goto case 2;
                            }
                            switch (removeSwitcher)
                            {
                                case 1:
                                    {
                                        Console.Clear();
                                        Console.Write("Код книги: ");
                                        int code;
                                        int.TryParse(Console.ReadLine(), out code);
                                        if (code <= 0)
                                        {
                                            Console.WriteLine("Введено некорректное значение. Попробуйте еще раз!");
                                            Console.ReadKey();
                                            goto case 1;
                                        }
                                        var book = new Book() {Code = code};
                                        homeLibrary.Remove(1, book);
                                        break;
                                    }
                                case 2:
                                    {
                                        Console.Clear();
                                        Console.Write("Фамилия автора: ");
                                        string aSurname = Console.ReadLine();
                                        if (aSurname == "")
                                        {
                                            Console.WriteLine("Введено некорректное значение. Попробуйте еще раз!");
                                            Console.ReadKey();
                                            goto case 2;
                                        }
                                        var book = new Book() { AuthorSurname = aSurname};
                                        homeLibrary.Remove(2, book);
                                        break;
                                    }
                                case 3:
                                    {
                                        Console.Clear();
                                        Console.Write("Год издания: ");
                                        int year;
                                        int.TryParse(Console.ReadLine(), out year);
                                        if (year <= 0)
                                        {
                                            Console.WriteLine("Введено некорректное значение. Попробуйте еще раз!");
                                            Console.ReadKey();
                                            goto case 3;
                                        }
                                        var book = new Book() {Year = year};
                                        homeLibrary.Remove(3, book);
                                        break;
                                    }
                                default:
                                    {
                                        Console.WriteLine("Введено неверное значение. Попробуйте еще раз!");
                                        Console.ReadKey();
                                        break;
                                    }
                            }
                            break;
                        }
                    case 3:
                        {
                            Console.Clear();
                            Console.Write("[1] - Сортировка по автору\n[2] - Сортировка по названию книги\n[3] - Сортировка по году издания\n[4] - Сортировка по количеству страниц\n");
                            int sortSwitcher;
                            int.TryParse(Console.ReadLine(), out sortSwitcher);
                            if (sortSwitcher <= 0)
                            {
                                Console.WriteLine("Введено некорректное значение. Попробуйте еще раз!");
                                Console.ReadKey();
                                goto case 3;
                            }
                            switch (sortSwitcher)
                            {
                                case 1:
                                    {
                                        Console.Clear();
                                        homeLibrary.Sort(1);
                                        break;
                                    }
                                case 2:
                                    {
                                        Console.Clear();
                                        homeLibrary.Sort(2);
                                        break;
                                    }
                                case 3:
                                    {
                                        Console.Clear();
                                        homeLibrary.Sort(3);
                                        break;
                                    }
                                case 4:
                                    {
                                        Console.Clear();
                                        homeLibrary.Sort(4);
                                        break;
                                    }
                                default:
                                    {
                                        Console.WriteLine("Введено неверное значение. Попробуйте еще раз!");
                                        Console.ReadKey();
                                        break;
                                    }
                            }
                            break;
                        }
                    case 4:
                        {
                            Console.Clear();
                            Console.Write("[1] - Поиск по фамилии автора\n[2] - Поиск по названию книги\n[3] - Поиск по году издания\n");
                            int searchSwitcher;
                            int.TryParse(Console.ReadLine(), out searchSwitcher);
                            if (searchSwitcher <= 0)
                            {
                                Console.WriteLine("Введено некорректное значение. Попробуйте еще раз!");
                                Console.ReadKey();
                                goto case 4;
                            }
                            switch (searchSwitcher)
                            {

                                case 1:
                                    {
                                        Console.Clear();
                                        Console.Write("Введите фамилию автора: ");
                                        string surname = Console.ReadLine();
                                        if (surname == "")
                                        {
                                            Console.WriteLine("Введено некорректное значение. Попробуйте еще раз!");
                                            Console.ReadKey();
                                            goto case 1;
                                        }
                                        Console.WriteLine("Результаты поиска:");
                                        foreach (Book i in homeLibrary.Search(1, surname)) Console.WriteLine(i.ToString());
                                        Console.WriteLine("\nПоиск завершен!");
                                        break;
                                    }
                                case 2:
                                    {
                                        Console.Clear();
                                        Console.Write("Введите название книги: ");
                                        string bookName = Console.ReadLine();
                                        if (bookName == "")
                                        {
                                            Console.WriteLine("Введено некорректное значение. Попробуйте еще раз!");
                                            Console.ReadKey();
                                            goto case 2;
                                        }
                                        Console.WriteLine("Результаты поиска:");
                                        foreach (Book i in homeLibrary.Search(2, bookName)) Console.WriteLine(i.ToString());
                                        Console.WriteLine("\nПоиск завершен!");
                                        break;
                                    }
                                case 3:
                                    {
                                        Console.Clear();
                                        Console.Write("Введите год издания книги: ");
                                        string year = Console.ReadLine();
                                        if (year == "")
                                        {
                                            Console.WriteLine("Введено некорректное значение. Попробуйте еще раз!");
                                            Console.ReadKey();
                                            goto case 3;
                                        }
                                        Console.WriteLine("Результаты поиска:");
                                        foreach (Book i in homeLibrary.Search(3, year)) Console.WriteLine(i.ToString());
                                        Console.WriteLine("\nПоиск завершен!");
                                        break;
                                    }
                                default:
                                    {
                                        Console.WriteLine("Введено неверное значение. Попробуйте еще раз!");
                                        break;
                                    }
                            }
                            Console.ReadKey();
                            break;
                        }
                    case 5:
                        {
                            Console.Clear();
                            homeLibrary.GetLibrary();
                            Console.ReadKey();
                            break;
                        }
                    case 6:
                        {
                            checkExit = true;
                            using (StreamWriter books = new StreamWriter("Книги.txt"))
                            {
                                foreach (var i in library)
                                    books.WriteLine(i.ToString());
                            }
                            break;
                        }
                    default:
                        {
                            Console.WriteLine("Введено неверное значение. Попробуйте еще раз!");
                            Console.ReadKey();
                            break;
                        }
                }
            }
        }
    }
}
