﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Домашняя_библиотека
{
    class HomeLibrary
    {
        public List<Book> library = new List<Book>();

        public HomeLibrary (List<Book> library)
        {
            this.library = library;
        }

        public void Add (Book book)
        {
            library.Add(book);
            Console.WriteLine("\nКнига добавлена!");
            Console.ReadKey();
        }

        public void Remove (int fieldNumber, Book book)
        {
            switch (fieldNumber)
            {
                case 1:
                    {
                        var result = from books in library
                                     where books.Code == book.Code
                                     orderby books.Code
                                     select books;
                        for (int i = 0; i < library.Count; i++)
                        {
                            foreach (var j in result)
                            {
                                if (library[i].Code == j.Code) library.Remove(j);
                            }
                        }
                        break;
                    }
                case 2:
                    {
                        for (int i = 0; i < library.Count; i++)
                        {
                            foreach (var j in Search(1, book.AuthorSurname))
                            {
                                if (library[i].AuthorSurname == j.AuthorSurname) library.Remove(j);
                            }
                        }
                        break;
                    }
                case 3:
                    {
                        for (int i = 0; i < library.Count; i++)
                        {
                            foreach (var j in Search(fieldNumber, book.Year.ToString()))
                            {
                                if (library[i].Year == j.Year) library.Remove(j);
                            }
                        }
                        break;
                    }
            }
            Console.WriteLine("\nУдаление завершено!");
            Console.ReadKey();
        }

        public IOrderedEnumerable<Book> Search (int numberField, string template)
        {
            switch (numberField){
                case 1:
                    {
                        var result = from book in library
                                     where book.AuthorSurname == template
                                     orderby book.Code
                                     select book;
                        return result;
                    }
                case 2:
                    {
                        var result = from book in library
                                     where book.BookName == template
                                     orderby book.Code
                                     select book;
                        return result;
                    }
                case 3:
                    {
                        var result = from book in library
                                     where book.Year == int.Parse(template)
                                     orderby book.Code
                                     select book;
                        return result;
                    }
                default: return null;
            }
        }

        public void Sort(int numberField)
        {
            Console.WriteLine("Результаты сортировки:");
            switch (numberField)
            {
                case 1:
                    {
                        var result = from book in library
                                     orderby book.AuthorSurname, book.AuthorName, book.AuthorPatronymic
                                     select book;
                        foreach (Book i in result) Console.WriteLine(i.ToString());
                        break;
                    }
                case 2:
                    {
                        var result = from book in library
                                     orderby book.BookName
                                     select book;
                        foreach (Book i in result) Console.WriteLine(i.ToString());
                        break;
                    }
                case 3:
                    {
                        var result = from book in library
                                     orderby book.Year
                                     select book;
                        foreach (Book i in result) Console.WriteLine(i.ToString());
                        break;
                    }
                case 4:
                    {
                        var result = from book in library
                                     orderby book.PageNumber
                                     select book;
                        foreach (Book i in result) Console.WriteLine(i.ToString());
                        break;
                    }
            }
            Console.WriteLine("\nСортировка завершена!");
            Console.ReadKey();
        }

        public void GetLibrary()
        {
            if (library.Count == 0) Console.WriteLine("\nВ библиотеке пусто!");
            foreach (var i in library) Console.WriteLine(i.ToString());
        }

        public bool CheckCode(Book book)
        {
            foreach (var i in library)
                if (i.Code == book.Code) return true;
            return false;
        }

        public int CreateNewCode()
        {
            var result = from books in library
                         select books.Code;
            return result.Max() + 1;
        }
    }
}
