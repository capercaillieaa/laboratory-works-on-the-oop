﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Домашняя_библиотека
{
    class Book
    {
        public Book() { }
        public Book (int code, string bName)
        {
            Code = code;
            BookName = bName;
        }
        public Book(int code, string bName, string aName, string aSurname, string aPatronymic, int year, int page)
            : this(code, bName)
        {
            AuthorName = aName;
            AuthorSurname = aSurname;
            AuthorPatronymic = aPatronymic;
            Year = year;
            PageNumber = page;
        }
        public int Code { get; set; }
        public string AuthorName { get; set; }
        public string AuthorSurname { get; set; }
        public string AuthorPatronymic { get; set; }
        public string BookName { get; set; }
        public int Year { get; set; }
        public int PageNumber { get; set; }

        public override string ToString()
        {
            string answer = "";
            answer += Code.ToString() + "\t" + BookName;
            if (AuthorSurname != "") answer += "\t" + AuthorSurname;
            else answer += "\t" + "-";
            if (AuthorName != "") answer += "\t" + AuthorName;
            else answer += "\t" + "-";
            if (AuthorPatronymic != "") answer += "\t" + AuthorPatronymic;
            else answer += "\t" + "-";
            if (Year != 0) answer += "\t" + Year + "г.";
            else answer += "\t" + "-" + "г.";
            if (PageNumber != 0) answer += "\t" + PageNumber + "стр.";
            else answer += "\t" + "-" + "стр.";
            return answer;
        }
    }
}
