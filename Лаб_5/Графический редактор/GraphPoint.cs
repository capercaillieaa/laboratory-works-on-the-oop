﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Графический_редактор
{
    class GraphPoint : GraphFigure
    {
        public GraphPoint(Graphics field) : base(field)
        {
        }

        public float X { get; set; }
        public float Y { get; set; }

        public override void Draw(Color color, float border)
        {
            base.Draw(color, border);
            field.FillRectangle(new SolidBrush(graphColor), X, Y, borderStyle, borderStyle);
        }
    }
}
