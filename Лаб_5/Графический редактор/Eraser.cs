﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Графический_редактор
{
    class Eraser : GraphFigure
    {
        public Eraser(Graphics field) : base(field) { }

		public PointF Begin { get; set; }
		public PointF End { get; set; }

        public void Draw(Color color, float border, string checkStyle)
        {
            base.Draw(color, border);
			field.DrawLine(new Pen(graphColor, borderStyle), Begin, End);
			using (var brush = new SolidBrush(graphColor))
			{
				float offset = borderStyle * 0.5f;
				if (checkStyle == "Квадрат")
				{
					field.FillRectangle(brush, Begin.X - offset, Begin.Y - offset, borderStyle, borderStyle);
					field.FillRectangle(brush, End.X - offset, End.Y - offset, borderStyle, borderStyle);
				}
				else
				{
					field.FillEllipse(brush, Begin.X - offset, Begin.Y - offset, borderStyle, borderStyle);
					field.FillEllipse(brush, End.X - offset, End.Y - offset, borderStyle, borderStyle);
				}
			}
        }
    }
}
