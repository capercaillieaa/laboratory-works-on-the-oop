﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Imaging;

namespace Графический_редактор
{
    public partial class Form1 : Form
    {
        bool checkMouseDown = false;
        bool checkChange = false;
        string fileName = "";
        float border = 1;
        Graphics graph;
        Bitmap bmp;
        Point startPoint;
        Color borderColor = Color.Black, backColor = SystemColors.Control;

        public Form1()
        {
            InitializeComponent();
            bmp = new Bitmap(Field.Width, Field.Height);
            Field.Image = bmp;
            graph = Graphics.FromImage(bmp);
        }

        private void создатьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (checkChange)
            {
                if (MessageBox.Show("Сохранить изменения?", "Сохранение", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes) SaveBmp();
            }
            checkChange = false;
            fileName = "";
            Text = "Графический редактор";
            bmp = new Bitmap(Field.Width, Field.Height);
            Field.Image = bmp;
            graph = Graphics.FromImage(bmp);
        }

        private void открытьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PointIcon.Checked = LineIcon.Checked = RectIcon.Checked = EllipseIcon.Checked = EraserIcon.Checked = false;
            BrushIcon.Checked = true;
            if (checkChange)
            {
                if (MessageBox.Show("Сохранить изменения?", "Сохранение", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes) SaveBmp();  
            }
            openFileDialog.Filter = "BMP files (*.bmp)|*.bmp|" + "JPEG files (*.jpeg, *.jpg)|*.jpeg;*.jpg|" + "GIF files (*.gif)|*.gif|" + "All files (*.*)|*.*";
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                checkChange = false;
                fileName = openFileDialog.FileName;
                bmp = LoadBitmap(fileName);
                Text = "Текстовый редактор " + fileName;
                Field.Image = bmp;
                graph = Graphics.FromImage(bmp);
            }
        }

        public static Bitmap LoadBitmap(string fileName)
        {
            using (var bm = new Bitmap(fileName))
            {
                return new Bitmap(bm);
            }
        }

        private void сохранитьКакToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog.Filter = "BMP files (*.bmp)|*.bmp|" + "JPEG files (*.jpeg, *.jpg)|*.jpeg;*.jpg|" + "GIF files (*.gif)|*.gif|" + "All files (*.*)|*.*";
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                fileName = saveFileDialog.FileName;
                bmp.Save(fileName);
                checkChange = false;
            }
        }

        private void сохранитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveBmp();
            checkChange = false;
        }

        private void Field_MouseUp(object sender, MouseEventArgs e)
        {
            checkMouseDown = false;
            Field.Image = bmp;
            graph = Graphics.FromImage(bmp);
            if (PointIcon.Checked)
            {
                var point = new GraphPoint(graph);
                point.X = startPoint.X;
                point.Y = startPoint.Y;
                point.Draw(borderColor, border);
            }
            if (LineIcon.Checked)
            {
                var line = new GraphLine(graph);
                line.LineBeg = startPoint;
                line.LineEnd = new Point(e.X, e.Y);
                line.Draw(borderColor, border);
            }
            if (RectIcon.Checked)
            {
                var rect = new GraphRect(graph);
                rect.Corner1 = startPoint;
                rect.Corner2 = new Point(e.X, e.Y);
                rect.Draw(borderColor, backColor, border, FigureStyle.Text);
            }
            if (EllipseIcon.Checked)
            {
                var ellipse = new GraphEllipse(graph);
                ellipse.Corner1 = startPoint;
                ellipse.Corner2 = new Point(e.X, e.Y);
                ellipse.Draw(borderColor, backColor, border, FigureStyle.Text);
            }
			//TYT
            if (EraserIcon.Checked)
			{
				eraser = null;
			}
            if (BrushIcon.Checked)
            {
                brush = null;
            }
        }

        private void Field_MouseDown(object sender, MouseEventArgs e)
        {
            borderStyle.Text = border.ToString();
            checkMouseDown = true;
            startPoint.X = e.X;
            startPoint.Y = e.Y;
			//TYT
			if (EraserIcon.Checked)
			{
				eraser = new Eraser(graph);
				PreviousEraserPoint = new PointF(e.X, e.Y);
				eraser.Begin = eraser.End = PreviousEraserPoint;
				eraser.Draw(SystemColors.Control, border, EraserStyle.Text);
			}
            if (BrushIcon.Checked)
            {
                brush = new MyBrush(graph);
                PreviousEraserPoint = new PointF(e.X, e.Y);
                brush.Begin = brush.End = PreviousEraserPoint;
                brush.Draw(borderColor, border);
            }
		}

        private void PointIcon_Click(object sender, EventArgs e)
        {
            borderStyle.Text = border.ToString();
            if (!PointIcon.Checked) PointIcon.Checked = true;
            LineIcon.Checked = false;
            RectIcon.Checked = false;
            EllipseIcon.Checked = false;
            BrushIcon.Checked = false;
            EraserIcon.Checked = false;
            EraserLabel.Visible = EraserStyle.Visible = false;
            FigureStyleLabel.Visible = FigureStyle.Visible = false;
            toolStripSeparator2.Visible = false;
        }

        private void LineIcon_Click(object sender, EventArgs e)
        {
            borderStyle.Text = border.ToString();
            if (!LineIcon.Checked) LineIcon.Checked = true;
            PointIcon.Checked = false;
            RectIcon.Checked = false;
            EllipseIcon.Checked = false;
            BrushIcon.Checked = false;
            EraserIcon.Checked = false;
            EraserLabel.Visible = EraserStyle.Visible = false;
            FigureStyleLabel.Visible = FigureStyle.Visible = false;
            toolStripSeparator2.Visible = false;
        }

        private void RectIcon_Click(object sender, EventArgs e)
        {
            if (border > 5) borderStyle.Text = "5";
            else borderStyle.Text = border.ToString();
            FigureStyle.SelectedIndex = 0;
            if (!RectIcon.Checked) RectIcon.Checked = true;
            LineIcon.Checked = false;
            PointIcon.Checked = false;
            EllipseIcon.Checked = false;
            BrushIcon.Checked = false;
            EraserIcon.Checked = false;
            EraserLabel.Visible = EraserStyle.Visible = false;
            FigureStyleLabel.Visible = FigureStyle.Visible = true;
            toolStripSeparator2.Visible = true;
        }

        private void EllipseIcon_Click(object sender, EventArgs e)
        {
            if (border > 5) borderStyle.Text = "5";
            else borderStyle.Text = border.ToString();
            FigureStyle.SelectedIndex = 0;
            if (!EllipseIcon.Checked) EllipseIcon.Checked = true;
            LineIcon.Checked = false;
            PointIcon.Checked = false;
            RectIcon.Checked = false;
            BrushIcon.Checked = false;
            EraserIcon.Checked = false;
            EraserLabel.Visible = EraserStyle.Visible = false;
            FigureStyleLabel.Visible = FigureStyle.Visible = true;
            toolStripSeparator2.Visible = true;
        }

        private void Field_MouseClick(object sender, MouseEventArgs e)
        {
            checkChange = true;
        }

        private void опрограммеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Разработчик: Глушков Андрей Александрович", "О программе", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void SaveBmp()
        {
            saveFileDialog.Filter = "BMP files (*.bmp)|*.bmp|" + "JPEG files (*.jpeg, *.jpg)|*.jpeg;*.jpg|" + "GIF files (*.gif)|*.gif|" + "All files (*.*)|*.*";
            if (fileName == "")
            {
                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    fileName = saveFileDialog.FileName;
                    bmp.Save(fileName);
                }
            }
            else bmp.Save(fileName);
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (checkChange)
            {
                var answer = MessageBox.Show("Сохранить изменения?", "Сохранение", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                if (answer == DialogResult.Yes) SaveBmp();
                if (answer == DialogResult.Cancel) e.Cancel = true;
            }
        }

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void BorderColorButton_Click(object sender, EventArgs e)
        {
            if (colorDialog.ShowDialog() == DialogResult.OK)
            {
                borderColor = colorDialog.Color;
                BorderColorIcon.BackColor = colorDialog.Color;
            }
        }

        private void BackColorButton_Click(object sender, EventArgs e)
        {
            if (colorDialog.ShowDialog() == DialogResult.OK)
            {
                backColor = colorDialog.Color;
                BackColorIcon.BackColor = colorDialog.Color;
            }
        }

        private void borderStyle_DropDownClosed(object sender, EventArgs e)
        {
            border = int.Parse(borderStyle.Text);
        }

        private void borderStyle_TextChanged(object sender, EventArgs e)
        {
            int maxThickness;
            if (RectIcon.Checked || EllipseIcon.Checked) maxThickness = 5;
            else maxThickness = 50;
            try
            {
                if (borderStyle.Text[0] == '0') borderStyle.Text = borderStyle.Text.Remove(0, 1);
                if (int.Parse(borderStyle.Text) > maxThickness)
                    borderStyle.Text = maxThickness.ToString();
                if (int.Parse(borderStyle.Text) < 1)
                    borderStyle.Text = "1";
                border = int.Parse(borderStyle.Text);
            }
            catch
            {
                borderStyle.Text = "";
            }
        }

		//TYT
		PointF PreviousEraserPoint;
		Eraser eraser;
        MyBrush brush;

        private void Field_MouseMove(object sender, MouseEventArgs e)
        {
            Field.Image = bmp;
            graph = Graphics.FromImage(bmp);
            //TYT
            if (BrushIcon.Checked && checkMouseDown && brush !=null)
            {
                brush.Begin = PreviousEraserPoint;
                brush.End = new PointF(e.X, e.Y);
                brush.Draw(borderColor, border);
                PreviousEraserPoint = brush.End;
            }
			//TYT
            if (EraserIcon.Checked && checkMouseDown && eraser != null)
            {
				eraser.Begin = PreviousEraserPoint;
				eraser.End = new PointF(e.X, e.Y);
                eraser.Draw(SystemColors.Control, border, EraserStyle.Text);
				PreviousEraserPoint = eraser.End;
			}
        }

        private void BrushIcon_Click(object sender, EventArgs e)
        {
            borderStyle.Text = border.ToString();
            if (!BrushIcon.Checked) BrushIcon.Checked = true;
            LineIcon.Checked = false;
            PointIcon.Checked = false;
            EllipseIcon.Checked = false;
            RectIcon.Checked = false;
            EraserIcon.Checked = false;
            EraserLabel.Visible = EraserStyle.Visible = false;
            FigureStyleLabel.Visible = FigureStyle.Visible = false;
            toolStripSeparator2.Visible = false;
        }

        private void ClearIcon_Click(object sender, EventArgs e)
        {
            Field.Image = bmp;
            graph = Graphics.FromImage(bmp);
            graph.Clear(SystemColors.Control);
        }

        private void EraserIcon_Click(object sender, EventArgs e)
        {
            EraserStyle.SelectedIndex = 0;
            borderStyle.Text = border.ToString();
            if (!EraserIcon.Checked) EraserIcon.Checked = true;
            EraserLabel.Visible = true;
            EraserStyle.Visible = true;
            LineIcon.Checked = false;
            PointIcon.Checked = false;
            EllipseIcon.Checked = false;
            RectIcon.Checked = false;
            BrushIcon.Checked = false;
            toolStripSeparator2.Visible = true;
            FigureStyleLabel.Visible = FigureStyle.Visible = false;
        }

    }
}
