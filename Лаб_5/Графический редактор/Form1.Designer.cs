﻿namespace Графический_редактор
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.ImageList imageList;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.файлToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.создатьToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.открытьToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.сохранитьToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.сохранитькакToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.выходToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.справкаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.опрограммеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.PointIcon = new System.Windows.Forms.ToolStripButton();
            this.LineIcon = new System.Windows.Forms.ToolStripButton();
            this.RectIcon = new System.Windows.Forms.ToolStripButton();
            this.EllipseIcon = new System.Windows.Forms.ToolStripButton();
            this.BrushIcon = new System.Windows.Forms.ToolStripButton();
            this.EraserIcon = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.BorderColorIcon = new System.Windows.Forms.ToolStripTextBox();
            this.BorderColorButton = new System.Windows.Forms.ToolStripButton();
            this.BackColorButton = new System.Windows.Forms.ToolStripButton();
            this.BackColorIcon = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.borderStyle = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.FigureStyleLabel = new System.Windows.Forms.ToolStripLabel();
            this.FigureStyle = new System.Windows.Forms.ToolStripComboBox();
            this.EraserLabel = new System.Windows.Forms.ToolStripLabel();
            this.EraserStyle = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.ClearIcon = new System.Windows.Forms.ToolStripButton();
            this.Field = new System.Windows.Forms.PictureBox();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.colorDialog = new System.Windows.Forms.ColorDialog();
            imageList = new System.Windows.Forms.ImageList(this.components);
            this.menuStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Field)).BeginInit();
            this.SuspendLayout();
            // 
            // imageList
            // 
            imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            imageList.TransparentColor = System.Drawing.Color.Transparent;
            imageList.Images.SetKeyName(0, "Eraser.ico");
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.файлToolStripMenuItem1,
            this.справкаToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(839, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // файлToolStripMenuItem1
            // 
            this.файлToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.создатьToolStripMenuItem1,
            this.открытьToolStripMenuItem1,
            this.toolStripSeparator,
            this.сохранитьToolStripMenuItem1,
            this.сохранитькакToolStripMenuItem1,
            this.toolStripSeparator1,
            this.выходToolStripMenuItem});
            this.файлToolStripMenuItem1.Name = "файлToolStripMenuItem1";
            this.файлToolStripMenuItem1.Size = new System.Drawing.Size(48, 20);
            this.файлToolStripMenuItem1.Text = "&Файл";
            // 
            // создатьToolStripMenuItem1
            // 
            this.создатьToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("создатьToolStripMenuItem1.Image")));
            this.создатьToolStripMenuItem1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.создатьToolStripMenuItem1.Name = "создатьToolStripMenuItem1";
            this.создатьToolStripMenuItem1.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.создатьToolStripMenuItem1.Size = new System.Drawing.Size(172, 22);
            this.создатьToolStripMenuItem1.Text = "&Создать";
            this.создатьToolStripMenuItem1.Click += new System.EventHandler(this.создатьToolStripMenuItem_Click);
            // 
            // открытьToolStripMenuItem1
            // 
            this.открытьToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("открытьToolStripMenuItem1.Image")));
            this.открытьToolStripMenuItem1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.открытьToolStripMenuItem1.Name = "открытьToolStripMenuItem1";
            this.открытьToolStripMenuItem1.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.открытьToolStripMenuItem1.Size = new System.Drawing.Size(172, 22);
            this.открытьToolStripMenuItem1.Text = "&Открыть";
            this.открытьToolStripMenuItem1.Click += new System.EventHandler(this.открытьToolStripMenuItem_Click);
            // 
            // toolStripSeparator
            // 
            this.toolStripSeparator.Name = "toolStripSeparator";
            this.toolStripSeparator.Size = new System.Drawing.Size(169, 6);
            // 
            // сохранитьToolStripMenuItem1
            // 
            this.сохранитьToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("сохранитьToolStripMenuItem1.Image")));
            this.сохранитьToolStripMenuItem1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.сохранитьToolStripMenuItem1.Name = "сохранитьToolStripMenuItem1";
            this.сохранитьToolStripMenuItem1.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.сохранитьToolStripMenuItem1.Size = new System.Drawing.Size(172, 22);
            this.сохранитьToolStripMenuItem1.Text = "&Сохранить";
            this.сохранитьToolStripMenuItem1.Click += new System.EventHandler(this.сохранитьToolStripMenuItem_Click);
            // 
            // сохранитькакToolStripMenuItem1
            // 
            this.сохранитькакToolStripMenuItem1.Name = "сохранитькакToolStripMenuItem1";
            this.сохранитькакToolStripMenuItem1.Size = new System.Drawing.Size(172, 22);
            this.сохранитькакToolStripMenuItem1.Text = "Сохранить &как";
            this.сохранитькакToolStripMenuItem1.Click += new System.EventHandler(this.сохранитьКакToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(169, 6);
            // 
            // выходToolStripMenuItem
            // 
            this.выходToolStripMenuItem.Name = "выходToolStripMenuItem";
            this.выходToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.выходToolStripMenuItem.Text = "Вы&ход";
            this.выходToolStripMenuItem.Click += new System.EventHandler(this.выходToolStripMenuItem_Click);
            // 
            // справкаToolStripMenuItem
            // 
            this.справкаToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.опрограммеToolStripMenuItem});
            this.справкаToolStripMenuItem.Name = "справкаToolStripMenuItem";
            this.справкаToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.справкаToolStripMenuItem.Text = "Спра&вка";
            // 
            // опрограммеToolStripMenuItem
            // 
            this.опрограммеToolStripMenuItem.Name = "опрограммеToolStripMenuItem";
            this.опрограммеToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.опрограммеToolStripMenuItem.Text = "&О программе...";
            this.опрограммеToolStripMenuItem.Click += new System.EventHandler(this.опрограммеToolStripMenuItem_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.PointIcon,
            this.LineIcon,
            this.RectIcon,
            this.EllipseIcon,
            this.BrushIcon,
            this.EraserIcon,
            this.toolStripSeparator3,
            this.BorderColorIcon,
            this.BorderColorButton,
            this.BackColorButton,
            this.BackColorIcon,
            this.toolStripSeparator4,
            this.toolStripLabel1,
            this.borderStyle,
            this.toolStripSeparator6,
            this.FigureStyleLabel,
            this.FigureStyle,
            this.EraserLabel,
            this.EraserStyle,
            this.toolStripSeparator2,
            this.ClearIcon});
            this.toolStrip1.Location = new System.Drawing.Point(0, 24);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(839, 25);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // PointIcon
            // 
            this.PointIcon.CheckOnClick = true;
            this.PointIcon.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.PointIcon.Image = ((System.Drawing.Image)(resources.GetObject("PointIcon.Image")));
            this.PointIcon.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.PointIcon.Name = "PointIcon";
            this.PointIcon.Size = new System.Drawing.Size(23, 22);
            this.PointIcon.Text = "Точка";
            this.PointIcon.Click += new System.EventHandler(this.PointIcon_Click);
            // 
            // LineIcon
            // 
            this.LineIcon.CheckOnClick = true;
            this.LineIcon.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.LineIcon.Image = ((System.Drawing.Image)(resources.GetObject("LineIcon.Image")));
            this.LineIcon.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.LineIcon.Name = "LineIcon";
            this.LineIcon.Size = new System.Drawing.Size(23, 22);
            this.LineIcon.Text = "Линия";
            this.LineIcon.Click += new System.EventHandler(this.LineIcon_Click);
            // 
            // RectIcon
            // 
            this.RectIcon.CheckOnClick = true;
            this.RectIcon.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.RectIcon.Image = ((System.Drawing.Image)(resources.GetObject("RectIcon.Image")));
            this.RectIcon.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.RectIcon.Name = "RectIcon";
            this.RectIcon.Size = new System.Drawing.Size(23, 22);
            this.RectIcon.Text = "Прямоугольник";
            this.RectIcon.Click += new System.EventHandler(this.RectIcon_Click);
            // 
            // EllipseIcon
            // 
            this.EllipseIcon.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.EllipseIcon.Image = ((System.Drawing.Image)(resources.GetObject("EllipseIcon.Image")));
            this.EllipseIcon.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.EllipseIcon.Name = "EllipseIcon";
            this.EllipseIcon.Size = new System.Drawing.Size(23, 22);
            this.EllipseIcon.Text = "Эллипс";
            this.EllipseIcon.Click += new System.EventHandler(this.EllipseIcon_Click);
            // 
            // BrushIcon
            // 
            this.BrushIcon.Checked = true;
            this.BrushIcon.CheckOnClick = true;
            this.BrushIcon.CheckState = System.Windows.Forms.CheckState.Checked;
            this.BrushIcon.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BrushIcon.Image = ((System.Drawing.Image)(resources.GetObject("BrushIcon.Image")));
            this.BrushIcon.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BrushIcon.Name = "BrushIcon";
            this.BrushIcon.Size = new System.Drawing.Size(23, 22);
            this.BrushIcon.Text = "Кисть";
            this.BrushIcon.Click += new System.EventHandler(this.BrushIcon_Click);
            // 
            // EraserIcon
            // 
            this.EraserIcon.CheckOnClick = true;
            this.EraserIcon.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.EraserIcon.Image = ((System.Drawing.Image)(resources.GetObject("EraserIcon.Image")));
            this.EraserIcon.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.EraserIcon.Name = "EraserIcon";
            this.EraserIcon.Size = new System.Drawing.Size(23, 22);
            this.EraserIcon.Text = "Ластик";
            this.EraserIcon.Click += new System.EventHandler(this.EraserIcon_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // BorderColorIcon
            // 
            this.BorderColorIcon.BackColor = System.Drawing.SystemColors.WindowText;
            this.BorderColorIcon.Enabled = false;
            this.BorderColorIcon.Name = "BorderColorIcon";
            this.BorderColorIcon.Size = new System.Drawing.Size(22, 25);
            // 
            // BorderColorButton
            // 
            this.BorderColorButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BorderColorButton.Image = ((System.Drawing.Image)(resources.GetObject("BorderColorButton.Image")));
            this.BorderColorButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BorderColorButton.Name = "BorderColorButton";
            this.BorderColorButton.Size = new System.Drawing.Size(23, 22);
            this.BorderColorButton.Text = "Цвет";
            this.BorderColorButton.Click += new System.EventHandler(this.BorderColorButton_Click);
            // 
            // BackColorButton
            // 
            this.BackColorButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BackColorButton.Image = ((System.Drawing.Image)(resources.GetObject("BackColorButton.Image")));
            this.BackColorButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BackColorButton.Name = "BackColorButton";
            this.BackColorButton.Size = new System.Drawing.Size(23, 22);
            this.BackColorButton.Text = "Заливка";
            this.BackColorButton.Click += new System.EventHandler(this.BackColorButton_Click);
            // 
            // BackColorIcon
            // 
            this.BackColorIcon.Enabled = false;
            this.BackColorIcon.Name = "BackColorIcon";
            this.BackColorIcon.Size = new System.Drawing.Size(22, 25);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(65, 22);
            this.toolStripLabel1.Text = "Толщина: ";
            // 
            // borderStyle
            // 
            this.borderStyle.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.borderStyle.Name = "borderStyle";
            this.borderStyle.Size = new System.Drawing.Size(75, 25);
            this.borderStyle.Text = "1";
            this.borderStyle.TextChanged += new System.EventHandler(this.borderStyle_TextChanged);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 25);
            // 
            // FigureStyleLabel
            // 
            this.FigureStyleLabel.Name = "FigureStyleLabel";
            this.FigureStyleLabel.Size = new System.Drawing.Size(76, 22);
            this.FigureStyleLabel.Text = "Вид фигуры:";
            this.FigureStyleLabel.Visible = false;
            // 
            // FigureStyle
            // 
            this.FigureStyle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.FigureStyle.Items.AddRange(new object[] {
            "Без контура",
            "Прозрачная",
            "С заливкой"});
            this.FigureStyle.Name = "FigureStyle";
            this.FigureStyle.Size = new System.Drawing.Size(90, 25);
            this.FigureStyle.Visible = false;
            // 
            // EraserLabel
            // 
            this.EraserLabel.Name = "EraserLabel";
            this.EraserLabel.Size = new System.Drawing.Size(79, 22);
            this.EraserLabel.Text = "Вид ластика: ";
            this.EraserLabel.Visible = false;
            // 
            // EraserStyle
            // 
            this.EraserStyle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.EraserStyle.Items.AddRange(new object[] {
            "Квадрат",
            "Круг"});
            this.EraserStyle.Name = "EraserStyle";
            this.EraserStyle.Size = new System.Drawing.Size(75, 25);
            this.EraserStyle.Visible = false;
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            this.toolStripSeparator2.Visible = false;
            // 
            // ClearIcon
            // 
            this.ClearIcon.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ClearIcon.Image = ((System.Drawing.Image)(resources.GetObject("ClearIcon.Image")));
            this.ClearIcon.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ClearIcon.Name = "ClearIcon";
            this.ClearIcon.Size = new System.Drawing.Size(23, 22);
            this.ClearIcon.Text = "Очистить";
            this.ClearIcon.Click += new System.EventHandler(this.ClearIcon_Click);
            // 
            // Field
            // 
            this.Field.BackColor = System.Drawing.SystemColors.Control;
            this.Field.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Field.Location = new System.Drawing.Point(0, 49);
            this.Field.Name = "Field";
            this.Field.Size = new System.Drawing.Size(839, 495);
            this.Field.TabIndex = 2;
            this.Field.TabStop = false;
            this.Field.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Field_MouseClick);
            this.Field.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Field_MouseDown);
            this.Field.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Field_MouseMove);
            this.Field.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Field_MouseUp);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 544);
            this.Controls.Add(this.Field);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(300, 200);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Текстовый редактор";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Field)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton PointIcon;
        private System.Windows.Forms.ToolStripButton LineIcon;
        private System.Windows.Forms.ToolStripButton RectIcon;
        private System.Windows.Forms.PictureBox Field;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.ToolStripButton EllipseIcon;
        private System.Windows.Forms.ToolStripMenuItem файлToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem создатьToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem открытьToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator;
        private System.Windows.Forms.ToolStripMenuItem сохранитьToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem сохранитькакToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem выходToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem справкаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem опрограммеToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton BorderColorButton;
        private System.Windows.Forms.ToolStripButton BackColorButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripTextBox BorderColorIcon;
        private System.Windows.Forms.ToolStripTextBox BackColorIcon;
        private System.Windows.Forms.ColorDialog colorDialog;
        private System.Windows.Forms.ToolStripComboBox borderStyle;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripButton BrushIcon;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripButton ClearIcon;
        private System.Windows.Forms.ToolStripButton EraserIcon;
        private System.Windows.Forms.ToolStripComboBox EraserStyle;
        private System.Windows.Forms.ToolStripLabel EraserLabel;
        private System.Windows.Forms.ToolStripLabel FigureStyleLabel;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripComboBox FigureStyle;
    }
}

