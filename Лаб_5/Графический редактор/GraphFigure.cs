﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace Графический_редактор
{
    public abstract class GraphFigure
    {
        protected Color graphColor;
        protected float borderStyle;
        protected Graphics field;

        protected GraphFigure() { }
        protected GraphFigure(Graphics field)
        {
            this.field = field;
        }

        public virtual void Draw(Color color, float border)
        {
            graphColor = color;
            borderStyle = border;
        }
    }
}
