﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace Графический_редактор
{
    public class GraphLine : GraphFigure
    {
        public GraphLine(Graphics field) : base(field)
        {
        }

        public Point LineBeg { get; set; }
        public Point LineEnd { get; set; }

        public override void Draw(Color color, float border)
        {
            base.Draw(color, border);
            field.DrawLine(new Pen(graphColor, borderStyle), LineBeg, LineEnd);
        }
    }
}