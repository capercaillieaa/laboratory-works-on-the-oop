﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Графический_редактор
{
    class GraphRect : GraphFigure
    {
        public GraphRect(Graphics field) : base(field)
        {
        }

        public Point Corner1 { get; set; }
        public Point Corner2 { get; set; }

        public void Draw(Color color, Color backColor, float border, string figureStyle)
        {
            base.Draw(color, border);
            float x = Math.Min(Corner1.X, Corner2.X);
            float y = Math.Min(Corner1.Y, Corner2.Y);
            float width = Math.Abs(Corner2.X - Corner1.X);
            float height = Math.Abs(Corner2.Y - Corner1.Y);
            if (figureStyle == "Без контура")
                field.DrawRectangle(new Pen(backColor, borderStyle), x, y, width, height);
            else field.DrawRectangle(new Pen(color, borderStyle), x, y, width, height);
            if (figureStyle != "Прозрачная")
                field.FillRectangle(new SolidBrush(backColor), x + borderStyle / 2, y + borderStyle / 2, width - borderStyle, height - borderStyle);
        }
    }
}
