﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQ
{
    class Program
    {
        static void Main(string[] args)
        {
            int taskSwitcher = -1;
            while (taskSwitcher != 0)
            {
                Console.Clear();
                Console.WriteLine("[1] - Задание №1\n[2] - Задание №2\n[3] - Задание №3\n[4] - Задание №4\n" +
                "[5] - Задание №5\n[6] - Задание №6\n[7] - Задание №7\n[8] - Задание №8\n[9] - Задание №9\n[10] - Выход из программы");
                int.TryParse(Console.ReadLine(), out taskSwitcher);
                if (taskSwitcher == 0)
                {
                    Console.WriteLine("Введено неверное значение. Попробуйте еще раз!");
                    taskSwitcher = -1;
                    Console.ReadKey();
                    continue;
                }
                switch (taskSwitcher)
                {
                    case 1:
                        {
                            int a = 0, b = 0;
                            try
                            {
                                Input(ref a, ref b);
                                if (a < 1000 || b < 1000 || a > b) throw new ArgumentException();
                            }
                            catch
                            {
                                Console.WriteLine("Введены некорректные данные. Попробуйте еще раз!");
                                Console.ReadKey();
                                goto case 1;
                            }
                            var answer = Queries.Task1(a, b);
                            Console.WriteLine("Все числа - палиндромы от {0} до {1}: ", a, b);
                            OutputArray(answer);
                            break;
                        }
                    case 2:
                        {
                            int a = 0;
                            try
                            {
                                Input(ref a);
                                if (a < 0 || a > 1000000000) throw new ArgumentException();
                            }
                            catch
                            {
                                Console.WriteLine("Введены некорректные данные. Попробуйте еще раз!");
                                Console.ReadKey();
                                goto case 2;
                            }
                            Console.Write("Сумма цифр введенного числа: " + Queries.Task2(a));
                            break;
                        }
                    case 3:
                        {
                            int value = 0, digit = 0;
                            try
                            {
                                Input(ref value, ref digit);
                                if (value < 0 || digit < 0 || digit > 9) throw new ArgumentException();
                            }
                            catch
                            {
                                Console.WriteLine("Введены некорректные данные. Попробуйте еще раз!");
                                Console.ReadKey();
                                goto case 3;
                            }
                            Console.Write("Количество цифры {0} в числе {1}: {2}", digit, value, Queries.Task3(value, digit));
                            break;
                        }
                    case 4:
                        {
                            int a = 0;
                            try
                            {
                                Input(ref a);
                                if (a < 2 || a > 100000) throw new ArgumentException();
                            }
                            catch
                            {
                                Console.WriteLine("Введены некорректные данные. Попробуйте еще раз!");
                                Console.ReadKey();
                                goto case 4;
                            }
                            string answer = "Число " + a;
                            if (!Queries.Task4(a)) answer += " не";
                            answer += " является простым";
                            Console.WriteLine(answer);
                            break;
                        }
                    case 5:
                        {
                            int a = 0, b = 0;
                            try
                            {
                                Input(ref a, ref b);
                                if (a > b || a < 2 || b > 10000) throw new ArgumentException();
                            }
                            catch
                            {
                                Console.WriteLine("Введены некорректные данные. Попробуйте еще раз!");
                                Console.ReadKey();
                                goto case 5;
                            }
                            var arr = Queries.Task5(a, b);
                            Console.WriteLine("Все простые числа в диапазоне от {0} до {1}:", a, b);
                            OutputArray(arr);
                            break;
                        }
                    case 6:
                        {
                            int a = 0, b = 0;
                            try
                            {
                                Input(ref a, ref b);
                                if (a > b || a < 1 || b > 10000) throw new ArgumentException();
                            }
                            catch
                            {
                                Console.WriteLine("Введены некорректные данные. Попробуйте еще раз!");
                                Console.ReadKey();
                                goto case 6;
                            }
                            var arr = Queries.Task6(a, b);
                            Console.WriteLine("Числа с наибольшим количеством делителей в диапазоне от {0} до {1}: ", a, b);
                            OutputArray(arr);
                            break;
                        }
                    case 7:
                        {
                            try
                            {
                                var arr = Input();
                                if (arr == null) throw new ArgumentException();
                                var answer = Queries.Task7(arr);
                                Console.WriteLine("Числовой массив отсортирован по последним цифрам числа: ");
                                OutputArray(answer);
                            }
                            catch
                            {
                                Console.WriteLine("Введены некорректные данные. Попробуйте еще раз!");
                                Console.ReadKey();
                                goto case 7;
                            }
                            break;
                        }
                    case 8:
                        {
                            Console.Clear();
                            Console.Write("Введите строку: ");
                            string str = Console.ReadLine();
                            try
                            {
                                if (str == null) throw new ArgumentException();
                            }
                            catch
                            {
                                Console.WriteLine("Введены некорректные данные. Попробуйте еще раз!");
                                Console.ReadKey();
                                goto case 8;
                            }
                            Console.Write("Удаляем все цифры из строки: ");
                            Queries.Task8(str);
                            break;
                        }
                    case 9:
                        {
                            try
                            {
                                var arr = Input();
                                if (arr == null) throw new ArgumentException();
                                Console.WriteLine("Количество различных элементов: " + Queries.Task9(arr));
                            }
                            catch
                            {
                                Console.WriteLine("Введены некорректные данные. Попробуйте еще раз!");
                                Console.ReadKey();
                                goto case 9;
                            }
                            break;
                        }
                    case 10:
                        {
                            return;
                        }
                    default:
                        {
                            Console.WriteLine("Введено неверное значение. Попробуйте еще раз!");
                            break;
                        }
                }
                Console.ReadKey();
            }
        }

        static void Input(ref int a, ref int b)
        {
            Console.Clear();
            Console.Write("Введите число А: ");
            a = int.Parse(Console.ReadLine());
            Console.Write("Введите число B: ");
            b = int.Parse(Console.ReadLine());
        }

        static void Input(ref int a)
        {
            Console.Clear();
            Console.Write("Введите число А: ");
            a = int.Parse(Console.ReadLine());
        }

        static int[] Input()
        {
            Console.Clear();
            Console.Write("Введите количество элементов массива: ");
            int n;
            int.TryParse(Console.ReadLine(), out n);
            if (n == 0) return null;
            var arr = new int[n];
            Console.WriteLine("Введите элементы массива: ");
            for (int i = 0; i < n; i++)
            {
                int.TryParse(Console.ReadLine(), out arr[i]);
                if (arr[i] == 0)
                {
                    Console.WriteLine("Введенное значение некорректно. Продолжите ввод!");
                    i = i - 1;
                }
            }
            return arr;
        }

        static void OutputArray(int [] arr)
        {
            foreach (var i in arr) Console.Write(i + " ");
        }
    }
}
