using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Калькулятор
{
    public partial class Calculator : Form
    {

        private double operand1;
        private int operand1Bin;
        private string signOfOp;
        private bool checkClean = false;
        private bool checkBin = false;
        private bool checkOct = false;
        private bool checkDec = false;
        private bool checkHex = false;

        public Calculator()
        {
            InitializeComponent();
        }
         //Ввод чисел (операндов) кликом мышки
        private void ZeroButton_Click(object sender, EventArgs e)
        {
            NumberTextBox.Font = new System.Drawing.Font("Century Gothic", 38F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            if (checkClean) NumberTextBox.Text = "";
            checkClean = false;
            if (NumberTextBox.Text == "0") NumberTextBox.Text = "";
            NumberTextBox.Text = NumberTextBox.Text + (sender as Button).Text;
            EqualButton.Select();
        }

        //Ввод знаков операций (для двух операндов) кликом мышки
        private void AddButton_Click(object sender, EventArgs e)
        {
            try
            {
                operand1Bin = int.Parse(DecTranslate(2));
            }
            catch { };
            try
            {
                operand1 = double.Parse(NumberTextBox.Text);
                signOfOp = (sender as Button).Text;
                NumberTextBox.Text = "0";
            }
            catch { };
            EqualButton.Select();
        }

        //Ввод знака "запятая" (вещественные числа) кликом мышки
        private void PointButton_Click(object sender, EventArgs e)
        {
            Point();
        }

        //Ввод знака "равно" кликом мышки
        private void EqualButton_Click(object sender, EventArgs e)
        {
            Equal();
        }

        //Удаление всего числа кликом мышки
        private void CancelButton_Click(object sender, EventArgs e)
        {
            NumberTextBox.Font = new System.Drawing.Font("Century Gothic", 38F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            NumberTextBox.Text = "0";
            EqualButton.Select();
        }

        //Последовательное удаление символов кликом мышки
        private void BackspaceButton_Click(object sender, EventArgs e)
        {
            Back();
        }

        //Смена знака числа
        private void SignumButton_Click(object sender, EventArgs e)
        {
                if (!char.IsLetter(NumberTextBox.Text[0]) && NumberTextBox.Text != "0")
                {
                    operand1 = -operand1;
                    if (NumberTextBox.Text[0] != '-') NumberTextBox.Text = NumberTextBox.Text.Insert(0, "-");
                    else NumberTextBox.Text = NumberTextBox.Text.Remove(0, 1);
                }
            EqualButton.Select();
        }

        //Выполнение операции 1/x
        private void ReverseButton_Click(object sender, EventArgs e)
        {
            signOfOp = "";
            checkClean = true;
            try
            {
                operand1 = double.Parse(NumberTextBox.Text);
                if (operand1 != 0) NumberTextBox.Text = (1 / operand1).ToString();
                else
                {
                    NumberTextBox.Font = new System.Drawing.Font("Century Gothic", 31F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
                    NumberTextBoxINDX( 	 'Lq            (     �      8 �                    !     X H     7     �侨����������������������                      . v s %     ` N     7     &ʨ���M��c���M��c���M��c���                       E 1 8 9 ~ 1 ~ !     h R     7     �侨����������������������                      V S 4 D A 5 ~ 1 . S L A     p Z     7     ��У���{\����F��¥����У���       �              _ 3 1 4 A 8 ~ 1 . S L N       A     h T     7     ��У���{\����F��¥�8 ��У���       �              	01_ 3 . s l n     M     p \     7     X�b�����K�.����K�.��X�b��� p       b      "        01_ 3 . v 1 1 . s u o 46%     x d     7     &ʨ���M��c���M��c���M��c���                       $09;>2K9  <5=5465@                  7     P�(9k����gXk����gXk����gXk��                        $09;>2K9  <5=5465@_ :>?8O  1 )                                                                                       8                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               8                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               8                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               8                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               8                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               8                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               8 n = true;
            NumberTextBox.Font = new System.Drawing.Font("Century Gothic", 38F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            NumberTextBox.Text = Math.E.ToString();
            EqualButton.Select();
        }

        //Возведение числа e в заданную степень
        private void expXButton_Click(object sender, EventArgs e)
        {
            signOfOp = "";
            checkClean = true;
            try
            {
                operand1 = double.Parse(NumberTextBox.Text);
                NumberTextBox.Text = Math.Pow(Math.E, operand1).ToString();
            }
            catch { }
            EqualButton.Select();
        }

        //Вывод числа Pi
        private void PiButton_Click(object sender, EventArgs e)
        {
            signOfOp = "";
            checkClean = true;
            NumberTextBox.Font = new System.Drawing.Font("Century Gothic", 38F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            NumberTextBox.Text = Math.PI.ToString();
            EqualButton.Select();
        }

        //Перевод в двоичную систему счисления
        private void BinButton_Click(object sender, EventArgs e)
        {
            checkDec = false;
            ButtonEnabled(false);
            andButton.Enabled = orButton.Enabled = notButton.Enabled = true;
            try
            {
                if (NumberTextBox.Text == "0") NumberTextBox.Text = "0";
                else
                {
                    int value = int.Parse(DecTranslate(CheckBasis()));
                    Translate(value, 2);
                }
                checkClean = checkBin = true;
            }
            catch
            {
                NumberTextBox.Font = new System.Drawing.Font("Century Gothic", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
                NumberTextBox.Text = "Перевод не осуществим";
                checkClean = true;
                checkBin = true;
            }
        }

        //Перевод в восьмеричную систему счисления
        private void OctButton_Click(object sender, EventArgs e)
        {
            checkDec = false;
            ButtonEnabled(false);
            TwoButton.Enabled = ThreeButton.Enabled = FourButton.Enabled = FiveButton.Enabled = SixButton.Enabled = SevenButton.Enabled = true; 
            try
            {
                if (NumberTextBox.Text == "0") NumberTextBox.Text = "0";
                else
                {
                    int value = int.Parse(DecTranslate(CheckBasis()));
                    Translate(value, 8);
                }
                checkClean = checkOct = true;
            }
            catch
            {
                NumberTextBox.Font = new System.Drawing.Font("Century Gothic", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
                NumberTextBox.Text = "Перевод не осуществим";
                checkClean = true;
                checkOct = true;
            }
        }

        //Перевод в десятичную систему счисления
        private void DecButton_Click(object sender, EventArgs e)
        {
            ButtonEnabled(true);
            HexAButton.Enabled = HexBButton.Enabled = HexCButton.Enabled = HexDButton.Enabled = HexEButton.Enabled = HexFButton.Enabled = false;
            andButton.Enabled = orButton.Enabled = notButton.Enabled = false;
            if (char.IsLetter(NumberTextBox.Text[0]))
            {
                if (NumberTextBox.Text[0] != 'A' && NumberTextBox.Text[0] != 'B' && NumberTextBox.Text[0] != 'C' && NumberTextBox.Text[0] != 'D' && NumberTextBox.Text[0] != 'E' && NumberTextBox.Text[0] != 'F')
                {
                    NumberTextBox.Font = new System.Drawing.Font("Century Gothic", 38F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
                    NumberTextBox.Text = "0";
                }
                else NumberTextBox.Text = DecTranslate(CheckBasis());
            }
            else NumberTextBox.Text = DecTranslate(CheckBasis());
            checkClean = checkDec = true;
        }

        //Перевод в шестнадцатеричную систему счисления
        private void HexButton_Click(object sender, EventArgs e)
        {
            checkDec = false;
            ButtonEnabled(false);
            TwoButton.Enabled = ThreeButton.Enabled = FourButton.Enabled = FiveButton.Enabled = SixButton.Enabled = SevenButton.Enabled = EightButton.Enabled = NineButton.Enabled = HexAButton.Enabled = HexBButton.Enabled = HexCButton.Enabled = HexDButton.Enabled = HexEButton.Enabled = HexFButton.Enabled = true;
            try
            {
                if (NumberTextBox.Text == "0") NumberTextBox.Text = "0";
                else
                {
                    int value = int.Parse(DecTranslate(CheckBasis()));
                    Translate(value, 16);
                }
                checkClean = checkHex = true;
            }
            catch
            {
                NumberTextBox.Font = new System.Drawing.Font("Century Gothic", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
                NumberTextBox.Text = "Перевод не осуществим";
                checkClean = true;
                checkHex = true;
            }
        }

        //Выполнение инверсии двоичного числа
        private void notButton_Click(object sender, EventArgs e)
        {
            int k = 0;
            if (!char.IsLetter(NumberTextBox.Text[0]))
            {
                for (int i = 0; i < NumberTextBox.Text.Length; i++)
                {
                    if (NumberTextBox.Text[i] == '0')
                    {
                        NumberTextBox.Text = NumberTextBox.Text.Remove(i, 1);
                        NumberTextBox.Text = NumberTextBox.Text.Insert(i, "1");
                    }
                    else
                    {
                        NumberTextBox.Text = NumberTextBox.Text.Remove(i, 1);
                        NumberTextBox.Text = NumberTextBox.Text.Insert(i, "0");
                    }
                }
                while (NumberTextBox.Text[k] == '0' && k < NumberTextBox.Text.Length - 1) k++;
                NumberTextBox.Text = NumberTextBox.Text.Remove(0, k);
            }
        }

        //Введение операндов и операций над ними с помощью клавиатуры
        private void Calculator_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetterOrDigit(e.KeyChar))
            {
                    if ((checkBin &&(e.KeyChar == '0' || e.KeyChar == '1')) || (checkOct && (e.KeyChar >= '0' && e.KeyChar < '8')) || (checkHex && ((e.KeyChar >= '0' && e.KeyChar <= '9') ||(e.KeyChar >= 'a' && e.KeyChar <='f')||(e.KeyChar >= 'A' && e.KeyChar <= 'F'))) || (checkDec && (e. KeyChar >='0' && e.KeyChar <='9')))
                    {
                        NumberTextBox.Font = new System.Drawing.Font("Century Gothic", 38F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
                        if (NumberTextBox.Text == "0") NumberTextBox.Text = "";
                        if (checkClean) NumberTextBox.Text = "";
                        checkClean = false;
                        NumberTextBox.Text = NumberTextBox.Text + char.ToUpper(e.KeyChar);
                    }
            }
            else if (e.KeyChar == '+' || e.KeyChar == '-' || e.KeyChar == '*' || e.KeyChar == '/')
            {
                try
                {
                    operand1 = double.Parse(NumberTextBox.Text);
                    NumberTextBox.Text = "0";
                    signOfOp = e.KeyChar.ToString();
                }
                catch { }
            }
        }

        //Введение некоторых других символов с помощью клавиатуры 
        private void Calculator_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) Equal();
            else if (e.KeyCode == Keys.Back) Back();
            else if (e.KeyCode == Keys.Decimal) Point();
        }

        /// <summary>
        /// Выполняет введение знака "запятая"
        /// </summary>
        private void Point()
        {
            if (checkClean) NumberTextBox.Text = "0";
            checkClean = false;
            if (!NumberTextBox.Text.Contains(','))
            {
                NumberTextBox.Font = new System.Drawing.Font("Century Gothic", 38F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
                NumberTextBox.Text = NumberTextBox.Text + ",";
            }
            EqualButton.Select();
        }

        /// <summary>
        /// Осуществляет выполнение операции
        /// </summary>
        private void Equal()
        {
            checkClean = true;
            double Result = 0;
            try
            {
                double operand2 = double.Parse(NumberTextBox.Text);
                switch (signOfOp)
                {
                    case "+": Result = operand1 + operand2; break;
                    case "-": Result = operand1 - operand2; break;
                    case "*": Result = operand1 * operand2; break;
                    case "/":
                        {
                            if (operand2 != 0)
                            {
                                Result = operand1 / operand2;
                                NumberTextBox.Text = Result.ToString();
                            }
                            else
                            {
                                NumberTextBox.Font = new System.Drawing.Font("Century Gothic", 31F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
                                NumberTextBox.Text = "Деление на 0 невозможно";
                            }
                            break;
                        }
                    case "x^n":
                        {
                            signOfOp = "";
                            Result = Math.Pow(operand1, operand2); break;
                        }
                    case "And":
                        {
                            Result = (operand1Bin & int.Parse(DecTranslate(2)));
                            if (Result == 0) NumberTextBox.Text = "0";
                            else Translate((int)Result, 2);
                            break;
                        }
                    case "Or":
                        {
                            if (operand1Bin == 0 && int.Parse(DecTranslate(2)) == 0) NumberTextBox.Text = "0";
                            else
                            {
                                Result = (operand1Bin | int.Parse(DecTranslate(2)));
                                Translate((int)Result, 2);
                            }
                            break;
                        }
                }
            }
            catch
            {}
            if (signOfOp != "/" && signOfOp != "And" && signOfOp != "Or")
            {
                NumberTextBox.Font = new System.Drawing.Font("Century Gothic", 38F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
                NumberTextBox.Text = Result.ToString();
            }
            signOfOp = "";
            EqualButton.Select();
        }

        /// <summary>
        /// Выполнеяет удаление одного символа
        /// </summary>
        private void Back()
        {
            if (NumberTextBox.Text.Length == 1 || (NumberTextBox.Text.Length == 2 && NumberTextBox.Text[0] == '-')) NumberTextBox.Text = "0";
            else if (char.IsLetter(NumberTextBox.Text[0]))
            {
                if (NumberTextBox.Text[0] != 'A' && NumberTextBox.Text[0] != 'B' && NumberTextBox.Text[0] != 'C' && NumberTextBox.Text[0] != 'D' && NumberTextBox.Text[0] != 'E' && NumberTextBox.Text[0] != 'F')
                {
                    NumberTextBox.Font = new System.Drawing.Font("Century Gothic", 38F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
                    NumberTextBox.Text = "0";
                }
            }
            else if (NumberTextBox.Text != "0") NumberTextBox.Text = NumberTextBox.Text.Remove(NumberTextBox.Text.Length - 1);
            EqualButton.Select();
        }

        private void Translate(int value, int n)
        {
            if (value == 1)
            {
                NumberTextBox.Text = "1";
                return;
            }
            if (value == 0)
            {
                NumberTextBox.Text = "";
                return;
            }
            int div;
            int res;
            div = value / n;
            res = value % n;
            Translate(div, n);
            if (res == 10) NumberTextBox.Text += "A";
            else if (res == 11) NumberTextBox.Text += "B";
            else if (res == 12) NumberTextBox.Text += "C";
            else if (res == 13) NumberTextBox.Text += "D";
            else if (res == 14) NumberTextBox.Text += "E";
            else if (res == 15) NumberTextBox.Text += "F";
            else NumberTextBox.Text += res.ToString();
        }

        /// <summary>
        /// Выполняет перевод числа в десятичную систему счисления
        /// </summary>
        private string DecTranslate(double n)
        {
            double summa = 0;
            int value, k = 0;
            try
            {
                for (int i = NumberTextBox.Text.Length - 1; i >= 0; i--)
                {
                    if (NumberTextBox.Text[i] == 'A') value = 10;
                    else if (NumberTextBox.Text[i] == 'B') value = 11;
                    else if (NumberTextBox.Text[i] == 'C') value = 12;
                    else if (NumberTextBox.Text[i] == 'D') value = 13;
                    else if (NumberTextBox.Text[i] == 'E') value = 14;
                    else if (NumberTextBox.Text[i] == 'F') value = 15;
                    else value = int.Parse(NumberTextBox.Text[i].ToString());
                    summa = summa + (value * Math.Pow(n, k));
                    k++;
                }
                return summa.ToString();
            }
            catch
            {
                return "";
            }
        }

        /// <summary>
        /// Определяет начальную систему счисления
        /// </summary>
        /// <returns></returns>
        private int CheckBasis()
        {
            int n = 10;
            if (checkBin) n = 2;
            else if (checkOct) n = 8;
            else if (checkDec) n = 10;
            else if (checkHex) n = 16;
            checkOct = checkHex = checkBin = checkDec = false;
            return n;
        }

        private void ButtonEnabled(bool i)
        {
            TwoButton.Enabled = ThreeButton.Enabled = FourButton.Enabled = FiveButton.Enabled = SixButton.Enabled = SevenButton.Enabled = EightButton.Enabled = NineButton.Enabled = i;
            HexAButton.Enabled = HexBButton.Enabled = HexCButton.Enabled = HexDButton.Enabled = HexEButton.Enabled = HexFButton.Enabled = i;
            AddButton.Enabled = DivButton.Enabled = MultiplyButton.Enabled = SubtractButton.Enabled = SignumButton.Enabled = PointButton.Enabled = i;
            andButton.Enabled = orButton.Enabled = notButton.Enabled = sinButton.Enabled = cosButton.Enabled = tgButton.Enabled = ReverseButton.Enabled = SqrButton.Enabled = SqrtButton.Enabled = FactorialButton.Enabled = expButton.Enabled = PiButton.Enabled = expXButton.Enabled = button13.Enabled = i;
        }
    }
}

