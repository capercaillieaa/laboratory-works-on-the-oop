﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UniversityClassLibrary;

namespace UniversityQueries
{
    class Program
    {
        static University university;
        static void Main(string[] args)
        {
            Load();
            Console.Clear();

            //Запрос №1
            var teachersList = new List<Teacher>();
            foreach (var i in university.faculties)
                foreach (var j in i.departments)
                    foreach (var k in j.teachers)
                        teachersList.Add(k);
            var answer1 = from teacher in teachersList
                         where teacher.GetCurrentYearExperience() > 20
                         select teacher;
            foreach (var i in answer1)
                Console.WriteLine("{0}\t{1}\t{2}\t{3}\t{4}\t{5}", i.Id, i.IdDepartment, i.GetFullName(), i.Age, i.Experience, i.Post);

            Console.WriteLine("Для продолжения нажмите любую клавишу...");
            Console.ReadKey();

            //Запрос №2
            var studentsList = new List<Student>();
            foreach (var i in university.faculties)
                foreach (var j in i.groups)
                    foreach (var k in j.students)
                        studentsList.Add(k);
            var answer2 = from student in studentsList
                     where student.GetCourse() == 4
                     select student;
            foreach (var i in answer2)
                Console.WriteLine("\n{0}\t{1}\t{2}\t{3}\t", i.Id, i.IdGroup, i.GetFullName(), i.GetCourse());

            Console.WriteLine("Для продолжения нажмите любую клавишу...");
            Console.ReadKey();

            //Запрос №3
            Console.Write("\nВведите номер факультета: ");
            int number = 0;
            while (number == 0)
                int.TryParse(Console.ReadLine(), out number);
            var faculties = from faculty in university.faculties
                         where faculty.Id == number
                         select faculty;
            int groupsCount = 0;
            foreach (var i in faculties)
                groupsCount += i.groups.Count;
            int departmentsCount = 0;
            foreach (var i in faculties)
                departmentsCount += i.departments.Count;
            int studentsCount = 0;
            foreach (var i in faculties)
                foreach (var j in i.groups)
                    studentsCount += j.students.Count;
            int teachersCount = 0;
            foreach (var i in faculties)
                foreach (var j in i.departments)
                    teachersCount += j.teachers.Count;
            Console.WriteLine("Количество учебных групп: {0}", groupsCount);
            Console.WriteLine("Количество кафедр: {0}", departmentsCount);
            Console.WriteLine("Количество студентов: {0}", studentsCount);
            Console.WriteLine("Количество преподавателей: {0}", teachersCount);

            Console.WriteLine("Для продолжения нажмите любую клавишу...");
            Console.ReadKey();

            //Запрос 4
            var teachers = from f in university.faculties
                           from d in f.departments
                           from t in d.teachers
                           orderby t.GetFullName()
                           group d by t.GetFullName()
                           ;
            foreach (var t in teachers)
            {
                Console.WriteLine($"{t.Key}:");
                foreach (var d in t)
                    Console.WriteLine(university.faculties[0].Title + " - " + d.Title);
                Console.WriteLine();
            }

            Console.WriteLine("Для продолжения нажмите любую клавишу...");
            Console.ReadKey();

            //Запрос №5
            foreach (var i in studentsList)
                foreach (var j in studentsList)
                    if (i.GetFullName() == j.GetFullName()) Console.WriteLine(i.GetFullName());

            Console.WriteLine("Для продолжения нажмите любую клавишу...");
            Console.ReadKey();

            //Запрос №6
            Console.Write("\nВведите номер факультета: ");
            number = 0;
            while (number == 0)
                int.TryParse(Console.ReadLine(), out number);
            faculties = from faculty in university.faculties
                            where faculty.Id == number
                            select faculty;
            int firstCourse = 0, secondCourse = 0, thirdCourse = 0, fourthCourse = 0;
            foreach (var i in faculties)
                foreach (var j in i.groups)
                {
                    firstCourse += j.students.Where(a => a.GetCourse() == 1).Count();
                    secondCourse += j.students.Where(a => a.GetCourse() == 2).Count();
                    thirdCourse += j.students.Where(a => a.GetCourse() == 3).Count();
                    fourthCourse += j.students.Where(a => a.GetCourse() == 4).Count();
                }
            Console.WriteLine("Количество студентов первого курса: {0}", firstCourse);
            Console.WriteLine("Количество студентов второго курса: {0}", secondCourse);
            Console.WriteLine("Количество студентов третьего курса: {0}", thirdCourse);
            Console.WriteLine("Количество студентов четвертого курса: {0}", fourthCourse);

            Console.ReadKey();
        }

        static private void Load()
        {
            Console.Clear();
            if (University.Load("D:/ВятГУ/2-ой курс/4 семестр/ООП/Лаб_9/UniversityQueries/bin/Debug/University.dat", ref university))
            {
                Console.WriteLine("Загрузка успешно завершена!\nДля продолжения нажмите любую клавишу...");
                Console.ReadKey();
            }
            else
            {
                Console.WriteLine("Во время загрузки произошла ошибка!\nДля продолжения нажмите любую клавишу...");
                Console.ReadKey();
            }
        }
    }
}
