// <copyright file="UniversityTest.cs" company="HP Inc.">Copyright © HP Inc. 2019</copyright>

using System;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UniversityClassLibrary;

namespace UniversityClassLibrary.Tests
{
    /// <summary>Этот класс содержит параметризованные модульные тесты для University</summary>
    [TestClass]
    [PexClass(typeof(University))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    public partial class UniversityTest
    {

        /// <summary>Тестовая заглушка для .ctor(Int32, Int32, String, String)</summary>
        [PexMethod]
        public University ConstructorTest(
            int id,
            int idHead,
            string title,
            string description
        )
        {
            University target = new University(id, idHead, title, description);
            return target;
            // TODO: добавление проверочных утверждений в метод UniversityTest.ConstructorTest(Int32, Int32, String, String)
        }

        /// <summary>Тестовая заглушка для Load(String, University&amp;)</summary>
        [PexMethod]
        public bool LoadTest(string fileName, ref University university)
        {
            bool result = University.Load(fileName, ref university);
            return result;
            // TODO: добавление проверочных утверждений в метод UniversityTest.LoadTest(String, University&)
        }

        /// <summary>Тестовая заглушка для Save(String, University&amp;)</summary>
        [PexMethod]
        public bool SaveTest(string fileName, ref University university)
        {
            bool result = University.Save(fileName, ref university);
            return result;
            // TODO: добавление проверочных утверждений в метод UniversityTest.SaveTest(String, University&)
        }
    }
}
