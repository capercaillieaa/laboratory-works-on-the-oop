// <copyright file="StudentsGroupTest.cs" company="HP Inc.">Copyright © HP Inc. 2019</copyright>

using System;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UniversityClassLibrary;

namespace UniversityClassLibrary.Tests
{
    /// <summary>Этот класс содержит параметризованные модульные тесты для StudentsGroup</summary>
    [TestClass]
    [PexClass(typeof(StudentsGroup))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    public partial class StudentsGroupTest
    {

        /// <summary>Тестовая заглушка для .ctor(Int32, Int32, String, String, Int32)</summary>
        [PexMethod]
        public StudentsGroup ConstructorTest(
            int id,
            int idHead,
            string title,
            string description,
            int idFaculty
        )
        {
            StudentsGroup target = new StudentsGroup(id, idHead, title, description, idFaculty);
            return target;
            // TODO: добавление проверочных утверждений в метод StudentsGroupTest.ConstructorTest(Int32, Int32, String, String, Int32)
        }

        /// <summary>Тестовая заглушка для Add(Student)</summary>
        [PexMethod]
        public bool AddTest([PexAssumeUnderTest] StudentsGroup target, Student student)
        {
            bool result = target.Add(student);
            return result;
            // TODO: добавление проверочных утверждений в метод StudentsGroupTest.AddTest(StudentsGroup, Student)
        }

        /// <summary>Тестовая заглушка для Remove(Student)</summary>
        [PexMethod]
        public bool RemoveTest([PexAssumeUnderTest] StudentsGroup target, Student student)
        {
            bool result = target.Remove(student);
            return result;
            // TODO: добавление проверочных утверждений в метод StudentsGroupTest.RemoveTest(StudentsGroup, Student)
        }
    }
}
