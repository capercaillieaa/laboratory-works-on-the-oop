// <copyright file="FacultyTest.cs" company="HP Inc.">Copyright © HP Inc. 2019</copyright>

using System;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UniversityClassLibrary;

namespace UniversityClassLibrary.Tests
{
    /// <summary>Этот класс содержит параметризованные модульные тесты для Faculty</summary>
    [TestClass]
    [PexClass(typeof(Faculty))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    public partial class FacultyTest
    {

        /// <summary>Тестовая заглушка для .ctor(Int32, Int32, String, String)</summary>
        [PexMethod]
        public Faculty ConstructorTest(
            int id,
            int idHead,
            string title,
            string description
        )
        {
            Faculty target = new Faculty(id, idHead, title, description);
            return target;
            // TODO: добавление проверочных утверждений в метод FacultyTest.ConstructorTest(Int32, Int32, String, String)
        }

        /// <summary>Тестовая заглушка для AddDepartment(Department)</summary>
        [PexMethod]
        public bool AddDepartmentTest([PexAssumeUnderTest] Faculty target, Department department)
        {
            bool result = target.AddDepartment(department);
            return result;
            // TODO: добавление проверочных утверждений в метод FacultyTest.AddDepartmentTest(Faculty, Department)
        }

        /// <summary>Тестовая заглушка для AddGroup(StudentsGroup)</summary>
        [PexMethod]
        public bool AddGroupTest([PexAssumeUnderTest] Faculty target, StudentsGroup group)
        {
            bool result = target.AddGroup(group);
            return result;
            // TODO: добавление проверочных утверждений в метод FacultyTest.AddGroupTest(Faculty, StudentsGroup)
        }

        /// <summary>Тестовая заглушка для RemoveDepartment(Department)</summary>
        [PexMethod]
        public bool RemoveDepartmentTest([PexAssumeUnderTest] Faculty target, Department department)
        {
            bool result = target.RemoveDepartment(department);
            return result;
            // TODO: добавление проверочных утверждений в метод FacultyTest.RemoveDepartmentTest(Faculty, Department)
        }

        /// <summary>Тестовая заглушка для RemoveGroup(StudentsGroup)</summary>
        [PexMethod]
        public bool RemoveGroupTest([PexAssumeUnderTest] Faculty target, StudentsGroup group)
        {
            bool result = target.RemoveGroup(group);
            return result;
            // TODO: добавление проверочных утверждений в метод FacultyTest.RemoveGroupTest(Faculty, StudentsGroup)
        }
    }
}
