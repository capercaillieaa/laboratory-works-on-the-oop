// <copyright file="StudentTest.cs" company="HP Inc.">Copyright © HP Inc. 2019</copyright>

using System;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UniversityClassLibrary;

namespace UniversityClassLibrary.Tests
{
    /// <summary>Этот класс содержит параметризованные модульные тесты для Student</summary>
    [TestClass]
    [PexClass(typeof(Student))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    public partial class StudentTest
    {

        /// <summary>Тестовая заглушка для .ctor(Int32, String, String, String, Int32, Int32)</summary>
        [PexMethod]
        public Student ConstructorTest(
            int id,
            string name,
            string middleName,
            string surname,
            int idGroup,
            int year
        )
        {
            Student target = new Student(id, name, middleName, surname, idGroup, year);
            return target;
            // TODO: добавление проверочных утверждений в метод StudentTest.ConstructorTest(Int32, String, String, String, Int32, Int32)
        }

        /// <summary>Тестовая заглушка для GetCourse()</summary>
        [PexMethod]
        public int GetCourseTest([PexAssumeUnderTest] Student target)
        {
            int result = target.GetCourse();
            return result;
            // TODO: добавление проверочных утверждений в метод StudentTest.GetCourseTest(Student)
        }
    }
}
