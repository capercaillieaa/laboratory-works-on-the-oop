// <copyright file="UserTest.cs" company="HP Inc.">Copyright © HP Inc. 2019</copyright>

using System;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UniversityClassLibrary;

namespace UniversityClassLibrary.Tests
{
    /// <summary>Этот класс содержит параметризованные модульные тесты для User</summary>
    [TestClass]
    [PexClass(typeof(User))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    public partial class UserTest
    {

        /// <summary>Тестовая заглушка для GetFullName()</summary>
        [PexMethod]
        public string GetFullNameTest([PexAssumeNotNull] User target)
        {
            string result = target.GetFullName();
            return result;
            // TODO: добавление проверочных утверждений в метод UserTest.GetFullNameTest(User)
        }

        /// <summary>Тестовая заглушка для GetShortName()</summary>
        [PexMethod]
        public string GetShortNameTest([PexAssumeNotNull] User target)
        {
            string result = target.GetShortName();
            return result;
            // TODO: добавление проверочных утверждений в метод UserTest.GetShortNameTest(User)
        }
    }
}
