// <copyright file="DepartmentTest.cs" company="HP Inc.">Copyright © HP Inc. 2019</copyright>

using System;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UniversityClassLibrary;

namespace UniversityClassLibrary.Tests
{
    /// <summary>Этот класс содержит параметризованные модульные тесты для Department</summary>
    [TestClass]
    [PexClass(typeof(Department))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    public partial class DepartmentTest
    {

        /// <summary>Тестовая заглушка для .ctor(Int32, Int32, String, String, Int32)</summary>
        [PexMethod]
        public Department ConstructorTest(
            int id,
            int idHead,
            string title,
            string description,
            int idFaculty
        )
        {
            Department target = new Department(id, idHead, title, description, idFaculty);
            return target;
            // TODO: добавление проверочных утверждений в метод DepartmentTest.ConstructorTest(Int32, Int32, String, String, Int32)
        }

        /// <summary>Тестовая заглушка для Add(Teacher)</summary>
        [PexMethod]
        public bool AddTest([PexAssumeUnderTest] Department target, Teacher teacher)
        {
            bool result = target.Add(teacher);
            return result;
            // TODO: добавление проверочных утверждений в метод DepartmentTest.AddTest(Department, Teacher)
        }

        /// <summary>Тестовая заглушка для Remove(Teacher)</summary>
        [PexMethod]
        public bool RemoveTest([PexAssumeUnderTest] Department target, Teacher teacher)
        {
            bool result = target.Remove(teacher);
            return result;
            // TODO: добавление проверочных утверждений в метод DepartmentTest.RemoveTest(Department, Teacher)
        }
    }
}
