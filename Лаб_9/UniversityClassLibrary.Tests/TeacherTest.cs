// <copyright file="TeacherTest.cs" company="HP Inc.">Copyright © HP Inc. 2019</copyright>

using System;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UniversityClassLibrary;

namespace UniversityClassLibrary.Tests
{
    /// <summary>Этот класс содержит параметризованные модульные тесты для Teacher</summary>
    [TestClass]
    [PexClass(typeof(Teacher))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    public partial class TeacherTest
    {

        /// <summary>Тестовая заглушка для .ctor(Int32, String, String, String, Int32, DateTime, DateTime, String)</summary>
        [PexMethod]
        public Teacher ConstructorTest(
            int id,
            string name,
            string middleName,
            string surname,
            int idDepartment,
            DateTime experience,
            DateTime age,
            string post
        )
        {
            Teacher target = new Teacher(id, name, middleName, surname, idDepartment, experience, age, post);
            return target;
            // TODO: добавление проверочных утверждений в метод TeacherTest.ConstructorTest(Int32, String, String, String, Int32, DateTime, DateTime, String)
        }

        /// <summary>Тестовая заглушка для GetCurrentAge()</summary>
        [PexMethod]
        public int GetCurrentAgeTest([PexAssumeUnderTest] Teacher target)
        {
            int result = target.GetCurrentAge();
            return result;
            // TODO: добавление проверочных утверждений в метод TeacherTest.GetCurrentAgeTest(Teacher)
        }

        /// <summary>Тестовая заглушка для GetCurrentYearExperience()</summary>
        [PexMethod]
        public int GetCurrentYearExperienceTest([PexAssumeUnderTest] Teacher target)
        {
            int result = target.GetCurrentYearExperience();
            return result;
            // TODO: добавление проверочных утверждений в метод TeacherTest.GetCurrentYearExperienceTest(Teacher)
        }
    }
}
