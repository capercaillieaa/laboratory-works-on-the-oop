﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UniversityClassLibrary;

namespace UniversityDataBase
{
    class Program
    {
        const string errorStr = "Введены некорректные данные. Попробуйте еще раз.";
        static University university = new University(1, 1, "ВятГУ", "Вятский Государственный университет");
        static bool result;
        static void Main(string[] args)
        {
            Load();
            int mainSwitcher = -1;
            while (mainSwitcher != 3)
            {
                Console.Clear();
                Console.WriteLine("[1] - Редактировать данные\n[2] - Сохранить данные\n[3] - Выход");
                result = int.TryParse(Console.ReadLine(), out mainSwitcher);
                if (!result)
                {
                    ErrorMessage();
                    continue;
                }
                switch (mainSwitcher)
                {
                    case 1:
                        {
                            Console.Clear();
                            Console.WriteLine("[1] - Добавить факультет\n[2] - Редактировать данные о факультете");
                            int facultySwitcher = -1;
                            result = int.TryParse(Console.ReadLine(), out facultySwitcher);
                            if (!result)
                            {
                                ErrorMessage();
                                goto case 1;
                            }
                            EditDataOfUniversity(facultySwitcher);
                            break;
                        }
                    case 2:
                        {
                            Console.Clear();
                            if (University.Save("D:/ВятГУ/2-ой курс/4 семестр/ООП/Лаб_9/UniversityQueries/bin/Debug/University.dat", ref university))
                            {
                                Console.WriteLine("Сохренине успешно завершено!");
                                Console.ReadKey();
                            }
                            else
                            {
                                Console.WriteLine("При сохранении произошла ошибка!");
                                Console.ReadKey();
                            }
                            break;
                        }
                    case 3:
                        {
                            break;
                        }
                    default:
                        {
                            ErrorMessage();
                            continue;
                        }
                }
            }
        }

        static private void Load()
        {
            Console.Clear();
            if (University.Load("D:/ВятГУ/2-ой курс/4 семестр/ООП/Лаб_9/UniversityQueries/bin/Debug/University.dat", ref university))
            {
                Console.WriteLine("Загрузка успешно завершена!\nДля продолжения нажмите любую клавишу...");
                Console.ReadKey();
            }
            else
            {
                Console.WriteLine("Во время загрузки произошла ошибка!\nДля продолжения нажмите любую клавишу...");
                Console.ReadKey();
            }
        }

        static private void EditDataOfUniversity(int facultySwitcher)
        {
            switch (facultySwitcher)
            {
                case 1:
                    {
                        int id = 0, idHead = 0;
                        string title = "", description = "";
                        InputDataAboutGroups("факультета", ref id, ref idHead, ref title, ref description);
                        var faculty = new Faculty(id, idHead, title, description);
                        try
                        {
                            university.faculties.Add(faculty);
                            Console.WriteLine("\nВставка успешно завершена!");
                        }
                        catch
                        {
                            Console.WriteLine("\nПри вставке возникла ошибка!");
                        }
                        Console.ReadKey();
                        break;
                    }
                case 2:
                    {
                        Console.Clear();
                        int idFaculty;
                        Console.Write("Введите номер факультета: ");
                        result = int.TryParse(Console.ReadLine(), out idFaculty);
                        if (!result)
                        {
                            ErrorMessage();
                            goto case 2;
                        }
                        if (SearchGroup(university, idFaculty) == null)
                        {
                            Console.WriteLine("\nДанного факультета не существует. Попробуйте ввести номер еще раз!");
                            Console.ReadKey();
                            goto case 2;
                        }
                        Console.Clear();
                        Console.WriteLine("[1] - Добавить учебную группу\n[2] - Удалить учебную группу\n[3] - Редактировать данные о группе\n[4] - Добавить кафедру\n[5] - Удалить кафедру\n[6] - Редактировать данные о кафедре");
                        int groupsSwitcher = -1;
                        result = int.TryParse(Console.ReadLine(), out groupsSwitcher);
                        if (!result)
                        {
                            ErrorMessage();
                            goto case 2;
                        }
                        EditDataOfFaculty(groupsSwitcher, idFaculty);
                        break;
                    }
            }

        }

        static private void EditDataOfFaculty(int groupsSwitcher, int idFaculty)
        {
            switch(groupsSwitcher)
            {
                case 1:
                    {
                        int id = 0, idHead = 0;
                        string title = "", description = "";
                        InputDataAboutGroups("группы", ref id, ref idHead, ref title, ref description);
                        id *= 10;
                        var studentsGroup = new StudentsGroup(id, idHead, title, description, idFaculty);
                        var faculty = SearchGroup(university, idFaculty);
                        foreach (var i in university.faculties)
                            if (i == faculty)
                            {
                                if (i.AddGroup(studentsGroup)) Console.WriteLine("\nВставка успешно завершена!");
                                else Console.WriteLine("\nПри вставке возникла ошибка!");
                                Console.ReadKey();
                            }
                        break;
                    }
                case 2:
                    {
                        Console.Clear();
                        int idGroup;
                        Console.Write("Введите номер группы: ");
                        result = int.TryParse(Console.ReadLine(), out idGroup);
                        if (!result)
                        {
                            ErrorMessage();
                            goto case 2;
                        }
                        var faculty = SearchGroup(university, idFaculty);
                        var group = SearchGroup(faculty, idGroup * 10);
                        if (group == null)
                        {
                            Console.WriteLine("\nДанной группы не существует. Попробуйте ввести номер еще раз!");
                            Console.ReadKey();
                            goto case 2;
                        }
                        foreach (var i in university.faculties)
                            if (i == faculty)
                            {
                                if (i.RemoveGroup(group as StudentsGroup)) Console.WriteLine("\nУдаление успешно завершено!");
                                else Console.WriteLine("\nПри удалении возникла ошибка!");
                                Console.ReadKey();
                            }
                        break;
                    }
                case 3:
                    {
                        Console.Clear();
                        int idGroup;
                        Console.Write("Введите номер группы: ");
                        result = int.TryParse(Console.ReadLine(), out idGroup);
                        if (!result)
                        {
                            ErrorMessage();
                            goto case 2;
                        }
                        var faculty = SearchGroup(university, idFaculty);
                        var group = SearchGroup(faculty, idGroup * 10);
                        if (group == null)
                        {
                            Console.WriteLine("\nДанной группы не существует. Попробуйте ввести номер еще раз!");
                            Console.ReadKey();
                            goto case 3;
                        }
                        Console.Clear();
                        Console.WriteLine("[1] - Добавить студента\n[2] - Удалить студента");
                        int studentSwitcher = -1;
                        result = int.TryParse(Console.ReadLine(), out studentSwitcher);
                        if (!result)
                        {
                            ErrorMessage();
                            goto case 2;
                        }
                        EditDataOfStudents(studentSwitcher, idFaculty, idGroup);
                        break;
                    }
                case 4:
                    {
                        int id = 0, idHead = 0;
                        string title = "", description = "";
                        InputDataAboutGroups("кафедры", ref id, ref idHead, ref title, ref description);
                        id = id * 10 + 1;
                        var department = new Department(id, idHead, title, description, idFaculty);
                        var faculty = SearchGroup(university, idFaculty);
                        foreach (var i in university.faculties)
                            if (i == faculty)
                            {
                                if (i.AddDepartment(department)) Console.WriteLine("\nВставка успешно завершена!");
                                else Console.WriteLine("\nПри вставке возникла ошибка!");
                                Console.ReadKey();
                            }
                        break;
                    }
                case 5:
                    {
                        Console.Clear();
                        int idDepartment;
                        Console.Write("Введите номер кафедры: ");
                        result = int.TryParse(Console.ReadLine(), out idDepartment);
                        if (!result)
                        {
                            ErrorMessage();
                            goto case 2;
                        }
                        var faculty = SearchGroup(university, idFaculty);
                        var department = SearchGroup(faculty, idDepartment * 10 + 1);
                        if (department == null)
                        {
                            Console.WriteLine("\nДанной кафедры не существует. Попробуйте ввести номер еще раз!");
                            Console.ReadKey();
                            goto case 2;
                        }
                        foreach (var i in university.faculties)
                            if (i == faculty)
                            {
                                if (i.RemoveDepartment(department as Department)) Console.WriteLine("\nУдаление успешно завершено!");
                                else Console.WriteLine("\nПри удалении возникла ошибка!");
                                Console.ReadKey();
                            }
                        break;
                    }
                case 6:
                    {
                        Console.Clear();
                        int idDepartment;
                        Console.Write("Введите номер кафедры: ");
                        result = int.TryParse(Console.ReadLine(), out idDepartment);
                        if (!result)
                        {
                            ErrorMessage();
                            goto case 2;
                        }
                        var faculty = SearchGroup(university, idFaculty);
                        var group = SearchGroup(faculty, idDepartment * 10 + 1);
                        if (group == null)
                        {
                            Console.WriteLine("\nДанной кафедры не существует. Попробуйте ввести номер еще раз!");
                            Console.ReadKey();
                            goto case 3;
                        }
                        Console.Clear();
                        Console.WriteLine("[1] - Добавить преподавателя\n[2] - Удалить преподавателя");
                        int teacherSwitcher = -1;
                        result = int.TryParse(Console.ReadLine(), out teacherSwitcher);
                        if (!result)
                        {
                            ErrorMessage();
                            goto case 2;
                        }
                        EditDataOfDepartments(teacherSwitcher, idFaculty, idDepartment);
                        break;
                    }
            }
        }

        static private void EditDataOfStudents(int studentSwitcher, int idFaculty, int idGroup)
        {
            switch (studentSwitcher)
            {
                case 1:
                    {
                        int id = 0, year = 0;
                        string name = "", middleName = "", surname = "";
                        InputDataAboutUsers("студента", ref id, ref name, ref middleName, ref surname);
                        while (year == 0)
                        {
                            Console.Write("Год(*): ");
                            result = int.TryParse(Console.ReadLine(), out year);
                        }
                        var student = new Student(id, name, middleName, surname, idGroup, year);
                        var faculty = SearchGroup(university, idFaculty);
                        var studentsGroup = SearchGroup(faculty, idGroup * 10);
                        foreach (var i in university.faculties)
                            if (i == faculty)
                            {
                                foreach (var j in i.groups)
                                {
                                    if (j == studentsGroup)
                                    {
                                        if (j.Add(student)) Console.WriteLine("\nВставка успешно завершена!");
                                        else Console.WriteLine("\nПри вставке возникла ошибка!");
                                        Console.ReadKey();
                                    }
                                }
                            }
                        break;
                    }
                case 2:
                    {
                        Console.Clear();
                        int id;
                        Console.Write("Введите номер студента: ");
                        result = int.TryParse(Console.ReadLine(), out id);
                        if (!result)
                        {
                            ErrorMessage();
                            goto case 2;
                        }
                        var faculty = SearchGroup(university, idFaculty);
                        var studentsGroup = SearchGroup(faculty, idGroup);
                        var student = SearchUser(studentsGroup, id);
                        if (student == null)
                        {
                            Console.WriteLine("\nДанного студента не существует. Попробуйте ввести номер еще раз!");
                            Console.ReadKey();
                            goto case 2;
                        }
                        foreach (var i in university.faculties)
                            if (i == faculty)
                            {
                                foreach (var j in i.groups)
                                {
                                    if (j == studentsGroup)
                                    {
                                        if (j.Remove(student as Student)) Console.WriteLine("\nУдаление успешно завершено!");
                                        else Console.WriteLine("\nПри удалении возникла ошибка!");
                                        Console.ReadKey();
                                    }
                                }
                            }
                        break;
                    }
            }
        }

        static private void EditDataOfDepartments(int teacherSwitcher, int idFaculty, int idDepartment)
        {
            switch(teacherSwitcher)
            {
                case 1:
                    {
                        int id = 0;
                        string name = "", middleName = "", surname = "", post = "";
                        DateTime experience, age;
                        InputDataAboutUsers("преподавателя", ref id, ref name, ref middleName, ref surname);
                        Console.Write("Дата приема на работу: ");
                        DateTime.TryParse(Console.ReadLine(), out experience);
                        Console.Write("Дата рождения: ");
                        DateTime.TryParse(Console.ReadLine(), out age);
                        Console.Write("Должность: ");
                        post = Console.ReadLine();
                        var teacher = new Teacher(id, name, middleName, surname, idDepartment, experience, age, post);
                        var faculty = SearchGroup(university, idFaculty);
                        var department = SearchGroup(faculty, idDepartment * 10 + 1);
                        foreach (var i in university.faculties)
                            if (i == faculty)
                            {
                                foreach (var j in i.departments)
                                {
                                    if (j == department)
                                    {
                                        if (j.Add(teacher)) Console.WriteLine("\nВставка успешно завершена!");
                                        else Console.WriteLine("\nПри вставке возникла ошибка!");
                                        Console.ReadKey();
                                    }
                                }
                            }
                        break;
                    }
                case 2:
                    {
                        Console.Clear();
                        int id;
                        Console.Write("Введите номер преподавателя: ");
                        result = int.TryParse(Console.ReadLine(), out id);
                        if (!result)
                        {
                            ErrorMessage();
                            goto case 2;
                        }
                        var faculty = SearchGroup(university, idFaculty);
                        var department = SearchGroup(faculty, idDepartment);
                        var teacher = SearchUser(department, id);
                        if (teacher == null)
                        {
                            Console.WriteLine("\nДанного студента не существует. Попробуйте ввести номер еще раз!");
                            Console.ReadKey();
                            goto case 2;
                        }
                        foreach (var i in university.faculties)
                            if (i == faculty)
                            {
                                foreach (var j in i.departments)
                                {
                                    if (j == department)
                                    {
                                        if (j.Remove(teacher as Teacher)) Console.WriteLine("\nУдаление успешно завершено!");
                                        else Console.WriteLine("\nПри удалении возникла ошибка!");
                                        Console.ReadKey();
                                    }
                                }
                            }
                        break;
                    }
            }
        }

        static private void ErrorMessage()
        {
            Console.WriteLine(errorStr);
            Console.ReadKey();
        }

        static private BaseGroup SearchGroup(BaseGroup baseGroup, int id)
        {
            if (baseGroup is University)
            {
                var facultiesList = (baseGroup as University).faculties;
                foreach (var i in facultiesList)
                    if (i.Id == id) return i;
                return null;
            }
            else if (baseGroup is Faculty)
            {
                var groupsList = (baseGroup as Faculty).groups;
                foreach (var i in groupsList)
                    if (i.Id == id) return i;
                var depList = (baseGroup as Faculty).departments;
                if (depList.Count == 0) return null;
                foreach (var i in depList)
                    if (i.Id == id) return i;
                return null;
            }
            else return null;
        }

        static private User SearchUser(BaseGroup baseGroup, int id)
        {
            if (baseGroup is StudentsGroup)
            {
                var studentsList = (baseGroup as StudentsGroup).students;
                foreach (var i in studentsList)
                    if (i.Id == id) return i;
                return null;
            }
            else if (baseGroup is Department)
            {
                var teachersList = (baseGroup as Department).teachers;
                foreach (var i in teachersList)
                    if (i.Id == id) return i;
                return null;
            }
            else return null;
        }

        static private void InputDataAboutGroups(string groupName, ref int id, ref int idHead, ref string title, ref string description)
        {
            Console.Clear();
            Console.WriteLine("Поля, обязательные для заполнения(*):");
            while (id == 0)
            {
                Console.Write("Номер {0}(*): ", groupName);
                int.TryParse(Console.ReadLine(), out id);
            }
            while (idHead == 0)
            {
                Console.Write("Номер главы {0}(*): ", groupName);
                int.TryParse(Console.ReadLine(), out idHead);
            }
            while (title == "")
            {
                Console.Write("Название {0}(*): ", groupName);
                title = Console.ReadLine();
            }
            Console.Write("Описание {0}: ", groupName);
            description = Console.ReadLine();
        }

        static private void InputDataAboutUsers(string userName, ref int id, ref string name, ref string middleName, ref string surname)
        {
            Console.Clear();
            Console.WriteLine("Поля, обязательные для заполнения(*):");
            while (id == 0)
            {
                Console.Write("Номер {0}(*): ", userName);
                int.TryParse(Console.ReadLine(), out id);
            }
            while (surname == "")
            {
                Console.Write("Фамилия {0}(*): ", userName);
                surname = Console.ReadLine();
            }
            while (name == "")
            {
                Console.Write("Имя {0}(*): ", userName);
                name = Console.ReadLine();
            }
            Console.Write("Отчетсво {0}: ", userName);
            middleName = Console.ReadLine();
            
        }
    }
}
