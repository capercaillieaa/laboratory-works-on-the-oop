﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace UniversityClassLibrary
{
    [Serializable]
    public class University : BaseGroup
    {
        static BinaryFormatter formatter = new BinaryFormatter();
        public List<Faculty> faculties = new List<Faculty>();

        public University(int id, int idHead, string title, string description) :
            base (id, idHead, title, description)
        { }

        public static bool Load(string fileName, ref University university)
        {
            try
            {
                using (Stream input = File.OpenRead(fileName))
                    university = (University)formatter.Deserialize(input);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool Save(string fileName, ref University university)
        {
            try
            {
                using (Stream output = File.OpenWrite(fileName))
                    formatter.Serialize(output, university);
                return true;
            }
            catch
            {
                return false;
            }
        }
        
    }
}
