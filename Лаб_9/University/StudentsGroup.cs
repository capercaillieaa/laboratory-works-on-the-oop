﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UniversityClassLibrary
{
    [Serializable]
    public class StudentsGroup : BaseGroup
    {
        public List<Student> students = new List<Student>();
        int IdFaculty;

        public StudentsGroup(int id, int idHead, string title, string description, int idFaculty) :
            base(id, idHead, title, description)
        {
            IdFaculty = idFaculty;
        }

        public bool Add(Student student)
        {
            try
            {
                students.Add(student);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Remove(Student student)
        {
            try
            {
                students.Remove(student);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
