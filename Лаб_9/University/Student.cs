﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UniversityClassLibrary
{
    [Serializable]
    public class Student : User
    {
        public int IdGroup, Year;

        public Student(int id, string name, string middleName, string surname, int idGroup, int year) :
            base(id, name, middleName, surname)
        {
            IdGroup = idGroup;
            Year = year;
        }

        public int GetCourse()
        {
            int result = DateTime.Today.Year - Year;
            if (DateTime.Today.Month >= 9) result += 1;
            return result;
        }
    }
}
