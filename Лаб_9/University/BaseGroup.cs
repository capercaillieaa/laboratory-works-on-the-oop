﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UniversityClassLibrary
{
    [Serializable]
    public abstract class BaseGroup
    {
        public int Id, IdHead;
        public string Title, Description;

        public BaseGroup(int id, int idHead, string title, string description)
        {
            Id = id;
            IdHead = idHead;
            Title = title;
            Description = description;
        }

        /*private User GetHead()
        {
            if (this is StudentsGroup)
            {
                var studentGroup = this as StudentsGroup;
                return (Student)studentGroup.students.Where(i => i.Id == IdHead);
            }
            else if (this is Department)
            {
                var department = this as Department;
                return (Teacher)department.teachers.Where(i => i.Id == IdHead);
            }
            else if (this is Faculty)
            {
                var faculty = this as Faculty;
            }
            else
            {
                var university = this as University;

            }
        }*/
    }
}
