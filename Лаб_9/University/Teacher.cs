﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UniversityClassLibrary
{
    [Serializable]
    public class Teacher : User
    {
        public int IdDepartment;
        public DateTime Experience, Age;
        public string Post;

        public Teacher(int id, string name, string middleName, string surname, int idDepartment, 
            DateTime experience, DateTime age, string post) : base (id, name, middleName, surname)
        {
            IdDepartment = idDepartment;
            Experience = experience;
            Age = age;
            Post = post;
        }

        public int GetCurrentYearExperience()
        {
            int result = DateTime.Today.Year - Experience.Year;
            if (Experience.Month > DateTime.Today.Month) result -= 1;
            else if (Experience.Month == DateTime.Today.Month)
            {
                if (Experience.Day > DateTime.Today.Day) result -= 1;
            }
            return result;
        }

        public int GetCurrentAge()
        {
            int result = DateTime.Today.Year - Age.Year;
            if (Age.Month > DateTime.Today.Month) result -= 1;
            else if (Age.Month == DateTime.Today.Month)
            {
                if (Age.Day > DateTime.Today.Day) result -= 1;
            }
            return result;
        }
    }
}
