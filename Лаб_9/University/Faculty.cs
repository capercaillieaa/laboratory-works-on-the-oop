﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UniversityClassLibrary
{
    [Serializable]
    public class Faculty : BaseGroup
    {
        public List<StudentsGroup> groups = new List<StudentsGroup>();
        public List<Department> departments = new List<Department>();

        public Faculty(int id, int idHead, string title, string description) :
            base(id, idHead, title, description)
        { }

        public bool AddGroup(StudentsGroup group)
        {
            try
            {
                groups.Add(group);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool RemoveGroup(StudentsGroup group)
        {
            try
            {
                groups.Remove(group);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool AddDepartment(Department department)
        {
            try
            {
                departments.Add(department);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool RemoveDepartment(Department department)
        {
            try
            {
                departments.Remove(department);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
