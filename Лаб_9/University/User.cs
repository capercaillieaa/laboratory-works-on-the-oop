﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UniversityClassLibrary
{
    [Serializable]
    abstract public class User
    {
        public int Id;
        string Name, MiddleName, Surname;

        public User(int id, string name, string middleName, string surname)
        {
            Id = id;
            Name = name;
            MiddleName = middleName;
            Surname = surname;
        }

        public string GetFullName()
        {
            return Surname + " " + Name + " " + MiddleName;
        }

        public string GetShortName()
        {
            return (Name == null || MiddleName == null) ? ""
                : $"{Surname} {Name.FirstOrDefault()}. {MiddleName.FirstOrDefault()}.";
        }
    }
}
