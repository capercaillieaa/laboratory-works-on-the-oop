﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UniversityClassLibrary
{
    [Serializable]
    public class Department : BaseGroup
    {
        public List<Teacher> teachers = new List<Teacher>();
        int IdFaculty;

        public Department(int id, int idHead, string title, string description, int idFaculty) :
            base(id, idHead, title, description)
        {
            IdFaculty = idFaculty;
        }

        public bool Add(Teacher teacher)
        {
            try
            {
                teachers.Add(teacher);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Remove(Teacher teacher)
        {
            try
            {
                teachers.Remove(teacher);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
